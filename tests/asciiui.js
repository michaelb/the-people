
var _ = require('underscore');
_.str = require('underscore.string');
var asciiui = require('../lib/asciiui');



var map = {
    offset: {x: 0, y:0}, // to crop map
    size: {height: 5, width: 6},
    terrain: [["OO","OO","OO","OO","OO"],["OO","OO","pf","OO","OO"],["OO","OO","pf","pf","OO"],["OO","pf","pf","pf","OO"],["OO","OO","pf","pf","OO"],["OO","OO","OO","pf","OO"]],
    depletion: {
        3: {
            2: {
                game: -2,
                forage: 'X',
            },
        },
    },
    regions: {
        3: {
            2: {
                player: 0,
            }
        }
    },
};

exports.render_color_map_zoom_1 = function (test) {
    var expected = ['{bg-cyan} {/}{bg-cyan} {/}{bg-cyan} {/}{bg-cyan} {/}{fg-green}.{/}{fg-green}^{/}{bg-cyan} {/}{bg-cyan} {/}',
                    '{fg-green}.{/}{fg-green}^{/}{fg-green}.{/}{fg-green}^{/}{fg-green}.{/}{fg-green}^{/}{fg-green}.{/}{fg-green}^{/}',
                    '{bg-cyan} {/}{bg-cyan} {/}{fg-green}.{/}{fg-green}^{/}{fg-green}.{/}{fg-green}^{/}{fg-green}.{/}{fg-green}^{/}',
                    '{bg-cyan} {/}{bg-cyan} {/}{bg-cyan} {/}{bg-cyan} {/}{bg-cyan} {/}{bg-cyan} {/}{bg-cyan} {/}{bg-cyan} {/}'].join("\n");


    var render = asciiui.map_render(map, {
            center_x: 3,
            center_y: 3,
            width: 4,
            height: 4,
            zoom: 1,
        }, {
            color: true,
            unicode: false,
        });

    // XXX disabled
    //test.equal(_.str.strip(render), _.str.strip(expected));

    //console.log(render);
    test.done();
};


exports.render_bw_map_zoom_1 = function (test) {
    var expected = [
                    '',
                    '    ^.  ',
                    '^..^^..^',
                    '  ^.^.^.',
                    '        '].join("\n");
        
    var render = asciiui.map_render(map, {
            center_x: 3,
            center_y: 3,
            width: 4,
            height: 4,
            zoom: 1,
        }, {
            color: false,
            unicode: false
        });
    test.equal(render, expected);

    //console.log(render);
    test.done();
};



exports.render_bw_map_zoom_4 = function (test) {
    var expected = [
                '                 ^.....         ',
                '                 ^^^^^.         ',
                '                 ^^....         ',
                '                                ',
                ' ......  ..^^^^  ^^^^..  .^^^^^ ',
                ' ^.^^..  ...^^^  ^^....  .....^ ',
                ' ^^^^^^  ....^^  ^^^...  ..^^^^ ',
                '                                ',
                '         ^^^^..  ^.....  ^^^^.. ',
                '         ^^....  ^^^^^.  ^^.... ',
                '         ^^^...  ^^....  ^^^... '].join("\n");
        
    var render = asciiui.map_render(map, {
            center_x: 3,
            center_y: 3,
            width: 4,
            height: 4,
            zoom: 4,
        }, {
            color: false,
            unicode: false
        });
    test.equal(_.str.strip(render), _.str.strip(expected));

    //console.log(render);
    test.done();
};




exports.render_bw_map_zoom_8 = function (test) {
    var expected = [
            '                             ^^..........               ',
            '                             ^^^^^^^.....               ',
            '                             ^^^^^^^^^...               ',
            '                             ^^^^.^^^^...               ',
            '                             ^^^...^^....               ',
            '                             ^^..........               ',
            '                             ^^^^^^^.....               ',
            '                                                        ',
            ' ............  ..^^^^^^^^^^  ^^^^^^^^^...  ..^^^^^^^^^^ ',
            ' ^....^^.....  .....^^^^^^^  ^^^^^^^^....  .......^^^^^ ',
            ' ^....^^^....  ....^^^^^^^^  ^^^^........  .........^^^ ',
            ' ^^..^^^^....  ......^^^^^^  ^^..........  ....^....^^^ ',
            ' ^^.^^^^^^^..  .........^^^  ^^^^........  ...^^^..^^^^ ',
            ' ^^^^^^^^^^^^  .......^^^^^  ^^^^^^^.....  ..^^^^^^^^^^ ',
            ' ^^^^^^^^^^^^  .........^^^  ^^^^^^^^^...  .......^^^^^ ',
            '                                                        ',
            '               ^^^^^^^^^...  ^^..........  ^^^^^^^^^... ',
            '               ^^^^^^^^....  ^^^^^^^.....  ^^^^^^^^.... ',
            '               ^^^^........  ^^^^^^^^^...  ^^^^........ ',
            '               ^^..........  ^^^^.^^^^...  ^^.......... ',
            '               ^^^^........  ^^^...^^....  ^^^^........ ',
            '               ^^^^^^^.....  ^^..........  ^^^^^^^..... ',
            '               ^^^^^^^^^...  ^^^^^^^.....  ^^^^^^^^^... ',
                ].join("\n");
        
    var render = asciiui.map_render(map, {
            center_x: 3,
            center_y: 3,
            width: 4,
            height: 4,
            zoom: 8,
        }, {
            color: false,
            unicode: false
        });
    test.equal(_.str.strip(render), _.str.strip(expected));

    //console.log(render);
    test.done();
};

