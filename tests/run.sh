#!/bin/bash

########################
# CD into up dir
MY_PATH="`dirname \"$0\"`"
MY_PATH="`( cd \"$MY_PATH\" && pwd )`"
cd $MY_PATH
cd ..
########################

set -e # die on err
source ~/.nvm/nvm.sh
nvm use 0.10
npm install

#node ./tests.js
#nodeunit ./tests.js

if [ -n "$1" ];
then
    ./node_modules/.bin/nodeunit ./tests/$1.js
else
    ./node_modules/.bin/nodeunit ./tests/
fi


