var _ = require('underscore');
_.str = require('underscore.string');

var core = require('../lib/dna/core');
var language = require('../lib/dna/language');

process.on('uncaughtException', function(err) {
    console.error(err.stack);
});



exports.basic_dna = function (test) {
    var dna = new core.DNA("d05e6d0d936ecc11f9d0");

    var a = dna.choice(['fa', 'de', 're', 'so']);
    //console.log("choice", a);
    var b = dna.choice(['fa', 'de', 're', 'so', 'la', 'ol', 'to'], 8);
    //console.log("choice", b);

    test.done();
};

exports.basic_dna_2 = function (test) {
    var dna = new core.DNA("d05e6d0d936ecc11f9d0");

    var a = dna.choice(['fa', 'de', 're', 'so'], true);
    //console.log("choice", a);
    var b = dna.choice(['fa', 'de', 're', 'so'], true);
    //console.log("choice", b);

    test.done();
};


exports.words = function (test) {
    var dstring = core.random_dna_string();
    console.log("dstring", dstring);
    var dna = new core.DNA(dstring);
    var lang = new language.Language(dna);
    console.log("----------- Start!");
    console.log(lang.get_word(0, 1));
    console.log(lang.get_word(5, 3));
    console.log(lang.get_word(10, 5));
    console.log("----------- MUTATE!");
    dna = dna.mutated();
    lang = new language.Language(dna);
    console.log(lang.get_word(0, 1));
    console.log(lang.get_word(5, 3));
    console.log(lang.get_word(10, 5));
    console.log("----------- MUTATE!");
    dna = dna.mutated();
    lang = new language.Language(dna);
    console.log(lang.get_word(0, 1));
    console.log(lang.get_word(5, 3));
    console.log(lang.get_word(10, 5));
    //console.log(lang.get_word(2, 5));
    //console.log(lang.get_word(3, 7));
    test.done();
};






