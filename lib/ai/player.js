var _ = require('underscore');
_.str = require('underscore.string');
var technology = require('../tech/technology');
var actions = require("../core/actions");

var temperments = {
    movement: {
        brownian: function (player, memory, state) {
            // moves in random direction every turn, probably strongest
            var region_id = 0;
            var d = _.str.chars("NSEW")[_.random(3)];
            return actions.Action.new_move(region_id, d);
        },
        slow: function (player, memory, state) {
            // every 2 turns move
            var region_id = 0;
            memory.will_move = !memory.will_move;
            //console.log("THIS IS MEMORY", memory);
            if (!memory.will_move) {
                return temperments.movement.brownian(player, memory, state);
            } else {
                return actions.Action.new_move(null, null);
            }
        },
        very_slow: function (player, memory, state) {
            // moves every 4 turns, desolates surroundings at own expense
            memory.will_move = ((memory.will_move||0)+1)%4;
            if (memory.will_move === 0) {
                return temperments.movement.brownian(player, memory, state);
            } else {
                return actions.Action.new_move(null, null);
            }
        },
        stubborn: function (player, memory, state) {
            // NEVER moves, will probably die, but not before destroying the
            // environment
            return actions.Action.new_move(null, null);
        },
        /*wanderer: function (player, memory, state) {
            // Keeps on going in one direction until it hits water, then will
            // wanter around, tends to avoid people
            return null;
        }*/
    }
};


var get_turn_data = function (ai, player, state) {
    /*
     * super minimal AI
     */

    // some defaults
    ai = ai || {};
    ai.options = ai.options || {movement: 'slow'};
    ai.memory = ai.memory || {};
    player.data.ai = ai;

    var turn = new actions.Turn(player);
    var techs = player.techs_available_for_purchase();

    if (techs.length > 0) {
        // we can buy something! lets just get the first thing for now, until
        // we add advisers
        turn.set_knowledge(techs[_.random(techs.length-1)]);
    }
    var movement = temperments.movement[ai.options.movement]
    turn.set(movement(player, ai.memory, state));

    return turn.to_data();
};

module.exports.get_turn_data = get_turn_data;
module.exports.temperments = temperments;

