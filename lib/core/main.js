var blessedui = require('../ui/blessedui');
var core = require('../core/core');
var _ = require('underscore');
var server = require('../server');


/* * Main Client Code */
var connect = function (opts) {

    var io = require('socket.io-client');
    var socket = io.connect(opts.address, {
            port: opts.port,
        });

    socket.on('connect', function () {
        console.info("successfully connected to server");


        // Create new UI
        var ui = new blessedui.UI({
                on_quit: function () {
                    process.exit(0);
                },
                on_new_game: function (new_game_settings) {
                    new_game_settings.connect_right_away = true;
                    socket.emit('game.create', new_game_settings);
                },
                unicode: true,
                color: true
            });
        ui.main_menu();
        _debug_ui = ui;

        /*
         * Main "loop": wait for next turn, display turn UI, callback with
         * actions, emit turn done event, rinse repeat
         */
        socket.on('game.client.turn_ready', function (game) {
            ui.turn_ui(game.player_id, game.state, function (actions_data) {
                socket.emit('game.turn_decided', actions_data);
            });
        });

        socket.on('game.client.next_turn_preparing', function () {
            // Computing next turn, show "please wait" info
            //console.log("Computing next turn, please wait...");
        });

        socket.on('game.client.next_turn_ready', function (){
            // Turn is ready, let's simply echo back a request for the next
            // turn
            socket.emit('game.turn_next');
        });

    });
};

var check_version = function () {
    var version = Number(process.version.match(/^v(\d+\.\d+)/)[1]);
    if (version > 0.10 && version < 1) {
        console.error("the-people-client requires at least node v0.10. Version in use: ", version);
        process.exit(1);
    }
};


exports.connect = connect;
exports.check_version = check_version;
exports.start_server = server.start_server;
