/*
 * Core logic for advancing game state, etc
 */
var _ = require('underscore');
_.str = require('underscore.string');
var Map = require('../map/map').Map;
var Player = require('../core/player').Player;
var actions = require('../core/actions');
var logic = require('../core/logic');

//var terrain = require('./terrain');

var generate_map = function (options, players) {
    var map = Map.new_random(options);
    var skip_list = [];
    for (var i in players) {
        var player = new Player(players[i]);
        var choice = map.choose_starting_location(skip_list);
        skip_list = skip_list.concat(choice.skip_list);
        player.add_region(map, choice);
        // map.set_region(choice, {player: player.id});
        // player.regions.push({x: choice.x, y: choice.y});
    }
    return map;
};


var new_game = function (opts) {
    var options = _.extend({map: {}}, opts);
    var state = {
        original_options: options,
        turn_number: 0,
        map: {},
        actions: {},
        players: {},
        next_player_id: 0
    };

    var free_tech = ['stone_age.herbalism', 'stone_age.carving'][_.random(1)];

    /*
    //_.extend({}, new_game_template);
    var starting_dict = {
        info: {
            name: 'Human',
            color: 'red'
        },
        id: 0,
        regions: [],
        explored: [],
        qualities: {
            rigidity: 0,
            inequality: 0,
        },
        culture_level: 0,
        points: {
            knowledge: 0,
            culture: 0,
            legacy: 0,
        },
        population: {
            free: 2
        },
        era: 'stone_age',
        technology: [ 'stone_age.the_people', free_tech ]
        //ai: { options: { movement: "very_slow", }, },
    };
    */

    /* Add initial player data */
    state.players[0] = Player.new_human_dict(0);
    //state.players[0] = Player.new_ai_dict(0);

    /* Add in a few AI players data */
    state.players[1] = Player.new_ai_dict(1);
    state.players[2] = Player.new_ai_dict(2);
    state.next_player_id = 3;

    state.map = generate_map(options.map, state.players).data;
    return state;
};

var game_state_trim = function (player_id, state) {
    // shallow clone
    var trimmed_state = _.clone(state);

    var player_data = state.players[player_id+0];
    if (!player_data) {
        console.error("THIS is dATA", player_data, state);
        throw "Player not found";
    }
    var player = new Player(player_data);

    // Get the relevant player
    // trim the vision
    var global_map = new Map(state.map);
    var map = Map.from_vision(global_map, player);

    // trim the players in contact with player
    //for (state.players) { map.data.regions }

    trimmed_state.map = map.data;

    return trimmed_state;
};

var next_game_state = function (state) {

    // loop through all players in the game, creating turn objects
    var turns = [];
    for (var i in state.players) {
        var player_data = state.players[i];

        if (player_data.is_dead) {
            continue;
        }

        var player = new Player(player_data);
        var turn_data = state.actions[player_data.id] || {};

        if (player.is_ai()) {
            turn_data = player.get_ai_turn_data();
        } 
        turn = new actions.Turn(player, turn_data)

        turns.push(turn);
    }


    // sort turns based on total player population (small pops move
    // first)
    turns = _.sortBy(turns, function (turn) {
            return turn.player.pop('total');
        });

    var map = new Map(state.map);

    // Now we have turns, we loop through them, applying each phase in order
    var scratch = {};
    for (var i in logic.phases) {
        var phase_function = logic.phases[i];
        for (var j in turns) {
            // loop through players
            var turn = turns[j];

            // scratch contains temporary data for each player that the phase
            // function can apply
            var player_scratch = scratch[turn.player.data.id] || {};

            // Apply the actual phase logic, changing the game state
            var messages = phase_function(state, turn, player_scratch);
            scratch[turn.player.data.id] = player_scratch;

            if (messages) {
                turn.player.add_messages(state.turn_number+1, messages);
            }
        }
    }

    for (var i in logic.global_phases) {
        var phase_function = logic.global_phases[i];
        phase_function(state, map);
    };

    // increment turn number
    state.turn_number++;

    return state;
};

var required_players_ready = function (state) {
    return [0];
};


module.exports.new_game = new_game;
module.exports.game_state_trim = game_state_trim;
module.exports.required_players_ready = required_players_ready;
module.exports.next_game_state = next_game_state ;




