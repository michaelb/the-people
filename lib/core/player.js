var _ = require('underscore');
_.str = require('underscore.string');
var technology = require('../tech/technology');
var Regime = require('../tech/regime').Regime;
var player_ai = require('../ai/player');

var Player = function (data) {
    this.data = data;
};



Player._new_dict = function (id) {
    // Start randomly with either Herbalism and Herb Doctor or Carving and Hunt
    // Leader
    var free = [
                {
                    tech: 'stone_age.herbalism',
                    regime: 'stone_age.herb_doctor',
                },
                {
                    tech: 'stone_age.carving',
                    regime: 'stone_age.hunt_leader',
                },
            ][_.random(1)];
    //var language = _.random(12357890);

    // TODO: need to have separate Player and Civ entites, so that a
    // human player can be merged into an AI player and have separate
    // legacies (scores). (That is, if a human player's civ gets
    // absorbed by an AI player, you get the option of a legacy penalty
    // of maybe 5 + whatever wonders the AI has built to then become the
    // "successor" civ and take over. Similarly, if your people starve,
    // you have the option of "haunting the ruins" and then taking over
    // the first AI to come into contact after 10 turns or something.)
    var starting_dict = {
        /* cosmetic info */

        id: id,
        regions: [],
        explored: [],
        qualities: {
            rigidity: 0,
            //rigidity: 10,
            inequality: 0,
        },
        //culture: { language: language, },
        culture_level: 0,
        points: {
            knowledge: 0,
            culture: 0,
            legacy: 0,
        },
        regime: {
            name: free.regime,
            marker: 0,
        },
        population: {
            free: 1
        },

        // tech stuff
        era: 'stone_age',
        technology: [ 'stone_age.the_people',
                //'stone_age.rituals:0',
                //'stone_age.weaving:0',
                //'stone_age.stone_tips:0',
                free.tech ]
        //ai: { options: { movement: "very_slow", }, },
    };

    return starting_dict;
};


Player.new_human_dict = function (id, info) {
    var o = Player._new_dict(id);
    o.info = _.extend({
            name: 'Human',
            color: 'red'
        }, info);
    return o;
};

Player.new_ai_dict = function (id, opts) {
    var o = Player._new_dict(id);
    o.info = {
            name: 'AI ' + id,
            color: ['green', 'blue', 'cyan', 'magenta'][id % 4]
        };

    // simple brownian movement
    o.ai = {
        options: {
            movement: "brownian",
        },
    };
    return o;
};

Player.new_ai_dict_split = function (id, other_player, population, map) {
    var o = this.new_ai_dict(id);

    // inherit technology
    o.technology = [].concat(other_player.data.technology);
    o.population = population;
    //o.culture.language = other_player.data.culture.language;
    o.qualities.rigidity = _.random(other_player.data.qualities.rigidity);
    o.info.color = "black";

    // select random movement attitude
    var m_opts = _.keys(player_ai.temperments.movement);
    o.ai.options.movement = m_opts[_.random(m_opts.length-1)];

    var possible_places = _.shuffle(other_player.get_tiles_in_vision());
    var region = null;
    for (var i in possible_places) {
        var c = possible_places[i];
        if (!map.is_impassible(c) &&
                    !map.get_region(c, null) &&
                    !map.is_out_of_bounds(c)) {
            region = possible_places[i];
            break;
        }
    }

    if (!region) {
        // means could not create, they "go into nothingness"
        return null;
    }

    var n_p = new Player(o);
    n_p.add_region(map, region);

    return o;
};


var AGES = {
    stone_age: {
        vision: 2
    },
    bronze_age: {
        vision: 3
    },
    early_classical_age: {
        vision: 4
    },
    classical_age: {
        vision: 5
    }
};

Player.prototype.get_vision_range = function () {
    return AGES[this.data.era].vision;
};

Player.prototype.add_region = function (map, c) {
    map.set_region(c, {player: this.data.id,
                color: this.data.info.color});
    this.data.regions.push({x: c.x, y: c.y});
};

Player.prototype.remove_region = function (map, c) {
    map.set_region(c, null);
    var found = null;
    for (var i in this.data.regions) {
        if (_.isEqual(this.data.regions[i], c)) {
            found = i;
        }
    }
    if (found) {
        delete this.data.regions[i];
        this.data.regions = _.compact(this.data.regions);
        return true;
    }
    return false;
};


Player.prototype.move_region = function (map, c1, c2) {
    map.set_region(c, {player: this.data.id});
    this.data.regions.push({x: c.x, y: c.y});
};

Player.prototype.get_explored_tiles = function () {
    if (!this.data.explored ||
            this.data.explored.length < 1) {
        // haven't explored anything yet, lets just explore here
        return this.get_tiles_in_vision();
    }

    return this.data.explored;
};

Player.prototype.get_tiles_in_vision = function (explored) {
    var seen = {};
    var tiles = explored || [];
    for (var i in tiles) {
        seen[tiles[i].x+","+tiles[i].y] = true;
    }

    var range = this.get_vision_range();
    for (var i in this.data.regions) {
        var r = this.data.regions[i];
        for (var x=-range; x <= +range; x++) {
            for (var y=-range; y <= +range; y++) {
                if (Math.abs(x) + Math.abs(y) <= range) {
                    if (!seen[x+","+y]) {
                        tiles.push({x:r.x+x, y:r.y+y});
                        seen[x+","+y] = true;
                    }
                }
            }
        }
    }
    return tiles;
};

Player.prototype.get_tasks = function () {
    // NOTE: right now it is returning task INFOS, not real tasks... >_>
    var tech_list = this.data.technology;
    var ret = [];
    for (var i in tech_list) {
        var tech = technology.get(tech_list[i]);
        var tasks = tech.get_tasks();
        for (var j in tasks) {
            var t = _.extend({name: tasks[j].name}, tasks[j].info);
            ret.push(t);
        }
    }
    return ret;
};

Player.prototype.get_bonus_assignments = function () {
    // check if there is athletics, and that there is someone assigned to it
    return 0;
};


Player.prototype.pop = function (type, humanized) {
    var pop = 0;
    if (type === 'total') {
        pop = _(this.data.population)
            .values().reduce(function(a, b) { return a+b; });
    } else {
        pop = this.data.population[type] || 0;
    }

    return pop * (humanized ? 31 : 1);
};

Player.prototype.is_ai = function () {
    return !!this.data.ai;
};

Player.prototype.get_ai_turn_data = function (state) {
    // blank turn / defaults for now
    return player_ai.get_turn_data(this.data.ai, this, state);
};

var BASE_ASSIGNMENTS = 1;

Player.prototype.get_assignments = function () {
    return BASE_ASSIGNMENTS + 
                ((this.data.qualities && this.data.qualities.rigidity) || 0)*1 +
                this.get_bonus_assignments()*1;
};

Player.prototype.unlocked_eras = function () {
    /*
     * All unlocked eras, included present era
     */
    var eras = [];
    for (var i in technology.era_list) {
        eras.push(technology.era_list[i]);
        if (this.data.era === era_name) {
            break;
        } 
    }
    return eras;
};

Player.prototype.techs_available_for_purchase = function () {
    /*
     * if the player has enough to buy something, returns a list of what they
     * can afford, otherwise []
     */
    var techs = technology.Tech.available(this.data.era, this.data.technology);
    var k = this.get("knowledge");
    return _.filter(techs, function (tech) {
                    return technology.get(tech).cost <= k; });
};

Player.prototype.get = function (name) {
    return (this.data.points &&
            this.data.points[name]) || 0;
};

Player.prototype.get_regime = function () {
    return new Regime(this.data.regime);
};

Player.prototype.max_pop = function (type) {
    var regime = this.get_regime();
    return regime.max_pop(type);
};

Player.prototype.add_messages = function (turn_number, messages) {
    // AIs never get messages
    if (this.is_ai()) {
        //return;
    }

    // get stuff ready
    this.data.messages = (this.data.messages || {});

    // append messages
    this.data.messages[turn_number] = (this.data.messages[turn_number] || []).concat(messages);
};




module.exports.Player = Player;

