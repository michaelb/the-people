var _ = require('underscore');
_.str = require('underscore.string');

var technology = require('../tech/technology');
var Map = require('../map/map').Map;
var helper = require('../util/helper');

var TYPE = {
    // Must be done in this order
    PRIORITY: 1,
    ASSIGNMENT: 2,
    //ALLOCATE_GRAIN: 3,
    MOVE: 4,
    //ATTACK: 5,

    // adds resource production to given building, must be specified every turn
    //PRODUCE_BUILDING: 6,
    PURCHASE_KNOWLEDGE: 7,
    //PURCHASE_CULTURE: 8,
};

var DIRECTIONS = {
    NORTH: 'N',
    SOUTH: 'S',
    EAST:  'E',
    WEST:  'W',
};

var TYPE_I = _.invert(TYPE);
var DIRECTIONS_I = _.invert(DIRECTIONS);

/*
 * EXAMPLE ACTIONS:
    this.data = {
        type: PRIORITY,
        tasks: ['oral_tradition', 'the_hunt', 'mining']
    }
    this.data = {
        type: ASSIGNMENT,
        workers: {
            free: {
                the_hunt: 1
            },
            slave: {
                mining: 1
            }
        }
    }
 *
 */

/*
 * Single action that a player might perform on a turn
 */
var Action = function (data) {
    this.data = data;
    this.type = this.data.type;
    this.is_action = true;
};

Action.prototype.get = function (name) {
    if (this.type === TYPE.ASSIGNMENT) {
        return (this.data.workers &&
                this.data.workers[name]) || 0;
    } else if (this.type === TYPE.PRIORITY && name === 'tasks') {
        var tasks = this.data.tasks;
        if (!tasks) {
            throw "Tasks is falsy";
        }
        return tasks;
    } else if (this.type === TYPE.PURCHASE_KNOWLEDGE && name === 'purchase') {
        return this.data.purchase || null;
    } else {
        throw "actions.js: Get '"+name+"' is meaningless for this type.";
    }
};

Action.prototype.swap_priorities = function (name_a, name_b) {
    if (!name_a || !name_b) {
        throw "actions.js: swap_priorities  null values.";
    }

    if (this.type === TYPE.PRIORITY) {
        var task_names = this.get('tasks');
        var i_a = _.indexOf(task_names, name_a);
        var i_b = _.indexOf(task_names, name_b);
        this.data.tasks[i_a] = name_b;
        this.data.tasks[i_b] = name_a;
    } else {
        throw "actions.js: swap_priorities  is meaningless for this type.";
    }
};

var defaults = {};
defaults[TYPE.PRIORITY] = function (player) {
    var tasks = player.get_tasks();
    var sorted = _.sortBy(tasks, function (task) {
            return task.low_priority ?  1000 :
                    (task.default_priority || 999); });
    return Action.new_priority(sorted);
};

defaults[TYPE.ASSIGNMENT] = function (player) {
    /*
    var tasks = player.get_tasks();
    var slaves = player.pop('slave');
    var slave_assignment = {};
    if (player.pop('slave') > 0) {
        var unwanted = _.sortBy(tasks, function (task) {
                return task.low_priority ? 1 : 0; });
        while (slaves > 0) {
            slaves =- task.slots.length;
            slave_assignment[task.slots.length];
        }
    }
    */
    return Action.new_assignment({});
};

// No move for default move
defaults[TYPE.MOVE] = function (player) {
    return Action.new_move(null, null); };


// No buy for default research
defaults[TYPE.PURCHASE_KNOWLEDGE] = function (player) {
    return Action.new_knowledge(null); };


Action.new_move = function (region_id, direction) {
    if (direction !== null && !DIRECTIONS_I[direction]) {
        throw "Unknown direction "+ direction;
    }

    return new Action({
        type: TYPE.MOVE,
        region_id: region_id,
        direction: direction
    });
};

Action.new_knowledge = function (tech, choice) {
    return new Action({ type: TYPE.PURCHASE_KNOWLEDGE, purchase: tech, choice: choice  });
};

Action.new_priority = function (tasks) {
    // Creates a new priority Action based on list of tasks
    return new Action({
        type: TYPE.PRIORITY,
        tasks: _.map(tasks, function (t) { return t.name; })
    });
};

Action.new_assignment = function (assignments) {
    // modify game state based on this action
    return new Action({
        type: TYPE.ASSIGNMENT,
        workers: assignments
    });
};

Action.prototype.available = function (player) {
    // return available actions for given player
};




/*
 * Collection object for all the actions a player performs
 */
var Turn = function (player, data) {
    this.data = data || {};
    this.player = player;
};

Turn.from_old = function (player, data) {
    //var trim = [ TYPE.PURCHASE_KNOWLEDGE, TYPE.PRIORITY ];
    var trim = [ TYPE.PURCHASE_KNOWLEDGE ];
    /*
     * Trims data about old stuff
     */
    var data = helper.deepclone(data);
    for (var i in trim) { 
        delete data[trim[i]];
    }

    var turn = new Turn(player, data);

    // now make sure there's the right number of techs
    var priority = turn.get_or_default(TYPE.PRIORITY);
    var default_priority = defaults[TYPE.PRIORITY](turn.player);
    var tasks = priority.get('tasks');
    var default_tasks = default_priority.get('tasks');


    // probably can speed up:
    var same = tasks.length === default_tasks.length &&
            tasks.length === _.union(default_tasks, tasks).length;
    // (presently doing the union thing because what if a task gets obseleted
    // at the same time a new task is introduced?)

    if (!same) {
        // sort default based on tasks length
        var sorted = _.sortBy(default_tasks, function (task_name) {
                var i = _(tasks).indexOf(task_name);
                return i === -1 ? 999 : i;
            });
        priority.data.tasks = sorted;
        turn.set(priority);
    }
    return turn;
};



Turn.prototype.check = function (type) {
    type = parseInt(type, 10);
    if (!TYPE_I[type]) {
        throw ("Unknown type" + type);
    }
    return type;
};

Turn.prototype.get = function (type) {
    type = this.check(type);
    var res = this.data[type] || null;

    if (res) {
    if (res.is_action) {
        // shouldn't happen... >_>
        res = res.data;
    }

        // wrap with action object
        res = new Action(res);
    }
    return res;
};

Turn.prototype.set = function (action) {
    if (action.is_action) {
        action = action.data;
    }
    return this.data[action.type] = action;
};

/*
Turn.prototype.get_plural = function (type) {
    // for move, attack, diplomacy?
    type = this.check(type);
    var val = this.data[type] || [];
    return _.isArray(val) ? val : [val];
};

Turn.prototype.add_plural = function (action) {
    // for move, attack, diplomacy?
    var list = this.get_plural(action.type);
    list.push(action);
    this.data[type] = list;
};
*/

Turn.prototype.get_or_default = function (type) {
    var val = this.get(type);
    return val || defaults[type](this.player);
};

Turn.prototype.clear = function (type) {
    type = this.check(type);
    // sets to default
    this.set(defaults[type](this.player));
};

Turn.prototype.add = function (action) {
    this.data[action.type] = action.data || action;
};

Turn.prototype.assert_correct = function () {
    // This the key function that keeps people from cheating, it makes sure
    // that the actions given are correct for the given player
    for (var i in this.data) {
    }
};

Turn.prototype.swap_task_priority = function (name_a, name_b) {
    //this.clear(TYPE.ASSIGNMENT);
    var priority = this.get_or_default(TYPE.PRIORITY);
    priority.swap_priorities(name_a, name_b);
    this.set(priority); // in case it was a default
};

Turn.prototype.get_free_assigned = function () {
    // todo need to skip slave assignments
    var assignment = this.get_or_default(TYPE.ASSIGNMENT);
    var total_assigned = _(assignment.get('free'))
        .map(function (value, key) { return value })
        .reduce(function (a, b) { return a + b }, 0);
    return total_assigned;
};

Turn.prototype.assign_workers = function (task_name, type, amount) {
    /*
     * task_name is the name of the task to assign to
     * type is the type of worker to assign
     * amount is amount of workers to assign
     */
    var assignment = this.get_or_default(TYPE.ASSIGNMENT);
    var workers = assignment.data.workers;
    workers[type] = workers[type] || {};
    workers[type][task_name] = (workers[type][task_name] || 0) + amount;

    if (workers[type][task_name] < 1) {
        // clean up if we are not assigning any
        delete workers[type][task_name];
    }

    this.set(assignment); // in case it was a default
};


Turn.prototype.to_data = function () {
    /*
     * Gets all values, or their default ones
     */
    var data = {};
    for (var t in TYPE_I) {
        data[t] = this.get_or_default(t);
    }
    return data;
};


Turn.prototype.set_knowledge = function (tech_name, choice) {
    var knowledge = this.get_or_default(TYPE.PURCHASE_KNOWLEDGE);
    var tech = technology.get(tech_name);

    if (tech.has_choice()) {
        tech_name += ":" + (choice||0);
    } 

    knowledge.data.purchase = tech_name;

    this.set(knowledge); // in case it was a default
};

Turn.prototype.get_task_list = function () {
    /* *****
     * Core function that determines assignment / priority logic
     */
    // Figures out task assignments based on Actions

    //  figure out assignments
    var priority = this.get_or_default(TYPE.PRIORITY);
    var assignment = this.get_or_default(TYPE.ASSIGNMENT);

    // use this 
    var population = helper.deepclone(this.player.data.population);

    // Lits of tasks based on priority assigned
    var task_names = priority.get('tasks');
    var task_list = [];
    var tasks_by_name = {};

    // first generate list & task Objs
    for (var i in task_names) {
        var name = task_names[i];
        var task = new technology.Task(name);
        task_list.push(task);
        tasks_by_name[name] = task;
    }

    // Now go through and apply the "force" assignments
    for (var pop_type in assignment.data.workers) {
        var assignments = assignment.data.workers[pop_type];
        for (var task_name in assignments) {
            var q = Math.min(assignments[task_name], population[pop_type]);
            var task = tasks_by_name[task_name];

            // assign, specifying "is_forced"
            var remaining = task.assign(pop_type, q, true);
            population[pop_type] = population[pop_type] - q + remaining;
        }
    }

    // Now just do the remaining population, assigned based on priority
    for (var pop_type in population) {
        // Now assign remaining pop
        for (var i in task_list) {
            var task = task_list[i];
            population[pop_type] = task.assign(pop_type, population[pop_type]);

            // already assigned all of this type, break out early
            //if (population[pop_type] < 1) { break; }
        }
    }

    return new technology.TaskList(task_list);
};

Turn.prototype.apply = function (type, state) {
    var action = this.get(type);
    if (!action) {
        // nothing specified
        return false;
    }

    var messages = [];

    // something specified
    if (type === TYPE.MOVE) {
        var region_id = action.data.region_id;
        if (!_.isNumber(region_id) || !action.data.direction) {
            // also not an action
            return false;
        }

        var c = this.player.data.regions[region_id];

        var new_location = {
                N: {x: c.x, y:c.y-1},
                S: {x: c.x, y:c.y+1},
                W: {x: c.x-1, y:c.y},
                E: {x: c.x+1, y:c.y},
            }[action.data.direction];

        // Now do the actual move
        var map = new Map(state.map);
        if (!_.isEmpty(map.get_region(new_location)) ||
                    map.is_impassible(new_location) ||
                    map.is_out_of_bounds(new_location)) {
            messages.push("You attempted to move, but "+
                            "your way was blocked.");
        } else {
            this.player.remove_region(map, c);
            this.player.add_region(map, new_location);
        }
    } else if (type === TYPE.PURCHASE_KNOWLEDGE) {
        if (!action.data.purchase) {
            // not really purchasing anything
            return false;
        }

        var tech = technology.get(action.data.purchase);
        if (this.player.data.points.knowledge >= tech.cost) {
            this.player.data.points.knowledge -= tech.cost;

            // add to tech list
            this.player.data.technology.push(action.data.purchase);
        } else {
            // can't afford, do nothing
            console.error("Can't afford tech!");
        }
    } else {
        throw  "Unknown type specified." + type;
    }

    return messages;
};

Turn.prototype.guess_outcome = function () {
    var tasklist = this.get_task_list();
    var totals = tasklist.get_totals();
};

module.exports.Turn = Turn;
module.exports.TYPE = TYPE;
module.exports.DIRECTIONS = DIRECTIONS;
module.exports.Action = Action;

