/*
 * Game rules are applied here
 */

var _ = require('underscore');
_.str = require('underscore.string');

var actions = require('../core/actions');
var random = require('../util/random');
var Map = require('../map/map').Map;
var terrain = require('../map/terrain');
var util_classes = require('../util/classes');
var random = require('../util/random');

var Regime = require('../tech/regime').Regime;
var Player = require('../core/player').Player;

var sum = function (o_a, o_b, prop) {
    o_a[prop] = Math.max(((o_a[prop] || 0) + (o_b[prop] || 0)), 0);
};

var apply_sum = function (o_a, o_b) {
    for (var prop in o_b) {
        sum(o_a, o_b, prop);
    }
};

var f = {
    setup: function (state, turn, scratch) {
        scratch.randomizer = new random.Randomizer();
        scratch.task_list = turn.get_task_list();
        scratch.adjustments = {};

        // lets just clear messages to begin with to reduce traffic, no need
        // for the history to be transmitted every time
        turn.player.data.messages = [];
    },

    move: function (state, turn, scratch) {
        return turn.apply(actions.TYPE.MOVE, state);
    },

    explore: function (state, turn, scratch) {
        if (turn.player.is_ai()) {
            // skip exploration for AIs
            return;
        }

        /* * Apply exploration */
        var explored = new util_classes.CoordSet(
                turn.player.get_explored_tiles());
        explored.add_all(turn.player.get_tiles_in_vision());
        turn.player.data.explored = explored.list();
    },

    produce: function (state, turn, scratch) {
        var messages = [], map=null;

        var add_totals = function (t) {
            if (!t) { return false; }

            // Handle resources:
            for (var r_name in terrain.resources) {
                if (!t[r_name]) {
                    continue;
                }

                var resource = terrain.resources[r_name];

                // something is happening with this resource, need to
                // deplete tile
                map = map || new Map(state.map);

                // flip value, since deplete evenly assumes positive value
                var value = -t[r_name];
                var failure = map.deplete_evenly(
                        turn.player.data.regions, r_name, value);

                if (!failure) {
                    messages.push("The people " +
                            resource.action + " " + value + " " + r_name);
                } else {
                    // unable to success, need to force workers who were
                    // assigned to task "unemployed" and recompute totals
                    return r_name;
                }
            }

            // note: at this point we can affect scratch, since we know for
            // sure these totals will be successfully added

            // compute total culture, knowledge
            sum(turn.player.data.points, t, 'culture');
            sum(turn.player.data.points, t, 'knowledge');
            //.culture += (t.culture || 0);
            //.knowledge += (t.knowledge || 0);

            // add in danger
            //scratch.danger = (scratch.danger || 0) + (t.danger||0);
            sum(scratch, t, 'danger');//scratch.danger = (scratch.danger || 0) + (t.danger||0);

            // add in food
            sum(scratch, t, 'food');//scratch.food = (scratch.food || 0) + (t.food||0);

            // Compute waste
            sum(scratch, t, 'waste');
            //scratch.waste = (scratch.waste || 0) + (t.waste || 0);
            // no failure
            return false;
        };

        // XXX XXX XXX BUG
        // if we have asterisk that affect scratch.adjustment, then we will do
        // it too many times

        var special_args = [turn, scratch];
        var totals = scratch.task_list.get_totals(special_args);
        //console.log("PRODUCTION PHASE", totals);
        var failure = add_totals(totals);
        var tries = 0;

        if (failure) {
            messages.push("{red-bg}Not enough " + failure +"!{/red-bg}");
        }

        while (failure && tries++ < 100) {
            // failed to get resource, force task_list to unemploy tasks
            // who require that resource, and recompute
            scratch.task_list.unemploy_by_resource(failure);
            totals = scratch.task_list.get_totals();
            failure = add_totals(totals, scratch);

            // every iteration increases waste as punishment
            scratch.waste++;
        }

        if (tries > 99) {
            console.error("FAILED TO FORCE UNEMPLOYMENT! :/");
        }

        var regime = turn.player.get_regime();
        add_totals(regime.info.effect.production, scratch);

        if (regime.is_next_change()) {
            add_totals(regime.info.change.production, scratch);
        }

        // add in overpopulation as waste to reduce population growth
        scratch.waste = (scratch.waste || 0) +
            Math.max(0, turn.player.pop('free') - turn.player.max_pop('free'));

        scratch.wasted = 0;
        if (scratch.waste) {
            // positive waste
            scratch.wasted = scratch.randomizer.base(scratch.waste, random.LOW);
        } 

        scratch.food = scratch.food - (scratch.wasted || 0);
        scratch.food = Math.max(0, scratch.food);
        messages.push("Food produced: "+scratch.food+
                " ("+scratch.wasted+" waste)");

        //var failure = add_totals(totals);
        //console.log("done! did fail?", failure);

        //if (failure) {
            // task_list.force_unemployment(r_name);
            // var totals = task_list.get_totals();
        //}

        // trigger production phase actions from task list, if any
        //totals = task_list.phase('produce', state, turn, scratch);
        //add_totals(totals);
        return messages;
    },
    battle: function (state, turn, scratch) {},

    regime: function (state, turn, scratch) {

        var messages = [];
        var regime = turn.player.get_regime();

        // For now, we will only population split
        var is_change = regime.increment();

        if (is_change) {
            // Check for emigration
            if (turn.player.pop('free') > turn.player.max_pop('free')) {
                var m = f.split(turn.player, 'free', state, scratch)
                messages = messages.concat(m);
            }

            // Check for slave revolt
            if (turn.player.pop('slave') > turn.player.max_pop('slave')) {
                var m = f.split(turn.player, 'slave', state, scratch)
                messages = messages.concat(m);
            }

            // check for max regions / separatist revolt
        }

        return messages;
    },

    split: function (player, type, state, scratch) {
        // "pseudo-phase" where we calculate the split
        var messages = []
        var difference = player.pop('free') - player.max_pop('free');
        var amount = scratch.randomizer.base(difference, random.LOW);
        if (amount < 1) {
            // doesnt even split at 0
            return [];
        }
        messages.push("During the regime change, " + amount + " of your excess "+
                    "free population decided to split off and go on their own.");

        // subtract pop from us
        player.data.population['free'] -= amount;
        scratch.food -= amount; // each brings along 1 food

        if (amount < 2) {
            // needs at least two pop to get an AI
            return messages;
        }
        var map = new Map(state.map);

        var player_dict = Player.new_ai_dict_split(
                        state.next_player_id, player, {'free': amount}, map);


        if (!player_dict) {
            // could not find a place to go, it just "vanishes"
            return messages;
        }

        state.players[state.next_player_id] = player_dict;
        state.next_player_id++;

        return messages;
    },


    population: function (state, turn, scratch) {
        var total_pop = turn.player.pop('total');
        var messages = [];
        if (scratch.danger && scratch.danger > 0) {
            var killed = scratch.randomizer.base(scratch.danger, random.LOW);
            total_pop -= killed;
            messages.push("Danger: "+killed+" population deaths.");

            // lets just put deaths automatically to free people, need to think
            // through this more
            turn.player.data.population.free -= killed;
        }

        // factor in adjustments
        if (scratch.adjustments.food_required) {
            total_pop += scratch.adjustments.food_required;
        }

        var growth = scratch.randomizer.base(scratch.food - total_pop, random.HIGH);

        // factor in adjustments
        if (scratch.adjustments.growth) {
            growth += scratch.adjustments.growth;
        }

        if (growth > 0) {
            messages.push("{green-fg}It was a time of plenty:{/green-fg} "
                    + growth + " population growth.");
        } else if (growth === 0) {
            messages.push("The people made just enough to get by.");
        } else {
            messages.push("{red-fg}FAMINE STRIKES THE LAND!{/red-fg} It was "+
                    "a hard time: " + (-growth) + " population starved.");
        }

        turn.player.data.population.free += growth;
        return messages;
    },

    purchase_knowledge: function (state, turn, scratch) {
        var m = turn.apply(actions.TYPE.PURCHASE_KNOWLEDGE, state);
        //m = m.concat(turn.apply(actions.TYPE.MOVE, state));
        return m;
    },


    mutate: function (state, turn, scratch) {
        // Civilization properties mutate:
        // 1. Language   (like race, based on proximity to other civs)
        // 2. Genetic    (based on growth, and proximity to other civs)
        // 3. Religion   (hm, i think maybe starting at classical age)
        //
        // These properties can dictate civ merger & rebellion during early
        // classical age
        //
        // The use of it is when racism becomes an option, racism cna be used
        // to enforce slavery, world domination, etc. (although racism is
        // possible without genetic diffs too, just harder)
        return [];
    },


    // GLOBAL PHASES!
    replenish: function (state, map) {
        map.replenish();
    },

    check_for_deaths: function (state, map) {
        for (var i in state.players) {
            var player = state.players[i];
            if (player.population.free < 1) {
                // kill player
                state.players[i].is_dead = true;
            }
        }
    },

    check_for_victory: function (state, map) {
        // hah...
    },


};


var phases = [
    f.setup, // just clerical stuff
    f.move,
    f.explore,
    f.produce,
    f.battle,
    f.regime,
    f.population,
    f.purchase_knowledge,
    f.mutate,
    //, f.replenish
];

var global_phases = [
    f.replenish,
    f.check_for_deaths,
    f.check_for_victory,
];

module.exports.phases = phases;
module.exports.global_phases = global_phases;

