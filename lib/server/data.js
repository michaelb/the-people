// save / load, either redis or not

//var redis     = require('redis');

var fs = require('node-fs');
var path = require('path');

var SavedGameBackendBase = function (opts) {
    if (!opts) { return; }
};

var FileSystemSaveBackend = function (opts) {
    SavedGameBackendBase.apply(this, opts);
    this.conf_path = path.join(this._get_home_path(), ".config", "thepeople");
};

FileSystemSaveBackend.prototype = new SavedGameBackendBase();

FileSystemSaveBackend.prototype._get_home_path = function () {
    return process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'];
};

var EXTENSION = ".tpsavedgame";


FileSystemSaveBackend.prototype._preppath = function (game_id, is_autosave) {
    var filename = game_id + EXTENSION;
    var saved_games_path = path.join(this.conf_path, is_autosave ? "autosaves" : "saves");
    // make the dirs necessary
    fs.mkdirSync(saved_games_path, null, true);
    var full_path = path.join(saved_games_path, filename);
    return full_path;
};


FileSystemSaveBackend.prototype._do_save = function (game_id, game_data_string, is_autosave) {
    var full_path = this._prepath(game_id, is_autosave);
    var fd = fs.openSync(full_path, "w+");
    fs.writeSync(fd, game_data_string);
    fs.closeSync(fd);
};

FileSystemSaveBackend.prototype._do_load = function (game_id, is_autosave) {
    var full_path = this._prepath(game_id, is_autosave);
    var fd = fs.openSync(full_path, "w+");
    var data = fs.readSync(fd, game_data_string);
    fs.closeSync(fd);
    return data;
};

/* Right now (essentially) just a key-value store, later need to properly use
 * redis / etc */
var GameTracker = function (opts) {
    this.backend = new FileSystemSaveBackend();
};

GameTracker.prototype.get = function (game_id) {
    var s = this.backend._do_load(game_id, false);
    return JSON.loads(data);
};

GameTracker.prototype.save = function (game_id, data, is_autosave) {
    this.backend._do_save(game_id, JSON.stringify(data), false);
};


module.exports.GameTracker = GameTracker;


