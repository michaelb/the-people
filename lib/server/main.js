/*
 * Server architecture
 */

var core = require('../core/core');
var data = require('./data');
var _ = require('underscore');


var uuid = function () {
    return _.random(0, 1125899906842623).toString(36).toUpperCase();
};

var _debug_ui = null;

/* global game state info */
var game_states = {};

/* global ready state info */
var ready_states = {};

var start_debug_repl = function () {
    var net = require("net"),
        repl = require("repl");

    net.createServer(function (socket) {
            var r = repl.start({
                prompt: "Debug TP> ",
                input: socket,
                output: socket
            }).on('exit', function() {
                socket.end();
            });
            r.context.all_states = game_states;
            r.context.ready_states = ready_states;
            r.context.state = _.values(game_states)[0];
            r.context.player = _.values(_.values(game_states)[0].players)[0];
            r.context.ui = _debug_ui;
            r.context.turn = _debug_ui && _debug_ui._debug_turn;
        }).listen(3001);
};


var setup_socketio = function (opts) {
    var listen_opts = {};
    if (!opts.verbose) {
        listen_opts.log = false;
    }

    if (opts.mock_socketio) {
        // Local-mode only, use fake socket io
        var io = require('mock-socket.io').listen(null, listen_opts);
        return io;
    } else {
        // Use real socket io

        var server = require('http').Server(),
            io = require('socket.io').listen(server, listen_opts);
        server.listen(opts.port, opts.address);
        return io;
    }
};

var start_server = function (opts) {

    start_debug_repl();
    //if (!opts.secure) { start_debug_repl(); }
    var io = setup_socketio(opts);

    var game_tracker = new data.GameTracker();

    io.sockets.on('connection', function (socket) {
        var game_id = null;
        console.log("client connected");

        /* * "game" keeps track of the client state */
        var game = {
            id: null,
            player_id: null,
        };

        var join_game = function (player_id, game_id) {
            // only one game at a time for now
            game.id = game_id;
            game.player_id = player_id;
            socket.game_id = game_id
            socket.join('game-'+game.id);
        };

        var turn_ready = function () {
            var game_state = game_states[game.id];
            player_id = game.player_id;

            // Presently only one player, player ID 0
            game_state = core.game_state_trim(player_id, game_state);

            socket.emit('game.client.turn_ready', {
                    player_id: game.player_id,
                    state: game_state
                });
        };

        var next_turn = function () {
            /*
             * core function that increments game state
             */
            io.sockets.in('game-'+game.id).emit('game.client.next_turn_preparing');
            game_states[game.id] = core.next_game_state(game_states[game.id]);

            // Now save tha game state

            // And emit that the game state changed
            io.sockets.in('game-'+game.id).emit('game.client.next_turn_ready');
        };


        socket.on('game.create', function (new_game_settings) {
            /*
             * Sets up new game
             */
            var game_state = core.new_game(new_game_settings);
            var game_id = uuid();
            game_states[game_id] = game_state;
            join_game(0, game_id);

            if (new_game_settings.connect_right_away) {
                turn_ready();
            }
        });

        socket.on('game.turn_next', function () {
            // simple returns next turn
            turn_ready()
        });

        socket.on('game.turn_decided', function (actions) {
            if (!game.id){
                console.error("received turn_decided while not connected to a game");
            }

            var game_state = game_states[game.id];
            game_state.actions[game.player_id] = actions;

            // mark as ready
            ready_states[game.id + ":" + game.player_id] = true;

            var required_ids = core.required_players_ready(game_state);

            // check if all ready
            for (var i in required_ids) {
                var id = required_ids[i];
                if (!ready_states[game.id+":"+id]){
                    // somebody isn't ready
                    return;
                }
            }

            // is ready!
            //
            // Clear all readys
            for (var i in required_ids) {
                var id = required_ids[i];
                ready_states[game.id+":"+id] = false;
            }
            next_turn();
        });

        socket.on('game.connect', function (desired_game_id, desired_player_id) {
            join_game(desired_player_id, desired_game_id);
            turn_ready();
        });

        socket.on('game.list', function (options) {
            // List existing game
            emit('game.client.list', {
                ids: _.keys(game_states)
            });
        });

        socket.on('saved_game.load', function (options) {
            // Load saved game
        });


        socket.on('saved_game.list', function (options) {
            // List existing game
        });

        socket.on('debug.get_game_state', function (id) {
            // Send entire game state
            socket.emit('debug.client.get_game_state', game_states[id]);
        });
    });
};

exports.start_server = start_server;

