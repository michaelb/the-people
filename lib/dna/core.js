var _ = require('underscore');
_.str = require('underscore.string');
var util_random = require('../util/random');
var util_helper = require('../util/helper');

var BASE = 16;

var DNA = function (string) {
    // DNA string should be a hex number
    this.string = string;
    this.chars = _.str.chars(string);
    this.vals = _.map(this.chars, function (c) { return parseInt(c, BASE); } );
    this.base_offset = 0;
    this._compute_stats();
};

var MAX_NUMBER_VALUE = 1000000;
DNA.prototype._compute_stats = function () {
    return;
    // some broad infrequently changing statistics, useful in determining
    // "major random" stuff
    this.counts = _.countBy(this.chars, function (a) { return a; });
    var sorted_numbers = _.sortBy(this.chars,
            function (a) { return parseInt(a, BASE); });
    this.number_value = parseInt(this.string, BASE) % MAX_NUMBER_VALUE;
    sorted_numbers.join('');
    console.log("THIS IS NUM", this.number_value);
};

DNA.prototype.reset = function () {
    this.base_offset = 0;
};

/*
 * Generates a random value up to (but not including) given number. Not at all
 * well distributed.
 *
 * "Offset" is an offset on the gene sequence
 */
DNA.prototype.random = function (less_than, offset_or_consume) {
    var offset = 0;

    // Pick which gene(s) selects this pseudo random value
    if (offset_or_consume === true) {
        // cycling offset, e.g. consume
        offset = this.base_offset;
    } else if (offset_or_consume) {
        offset = offset_or_consume;
    }

    var gene_index = offset % this.vals.length;
    var count = Math.ceil(less_than / BASE);
    var last_gene_index = (gene_index + count) % this.vals.length;
    if (offset_or_consume === true) {
        // consume how much was needed
        this.base_offset += count;
    }
    var val = 0;

    // loop through them to generate the random number
    while (gene_index !== last_gene_index) {
        val += this.vals[gene_index];
        // loop around gene index
        gene_index = (gene_index + 1) % this.vals.length;
    }


    return val % less_than;
};

// based on entire DNA
DNA.prototype.major_random = function (less_than, offset_or_consume) {
    return this.random(less_than, offset_or_consume);
    var offset = 0;
    // Pick which gene(s) selects this pseudo random value
    if (offset_or_consume === true) {
        // cycling offset, e.g. consume
        offset = this.base_offset;
        this.base_offset++;
    } else if (offset_or_consume) {
        offset = offset_or_consume;
    }

    // example values: 0.50390625 0.50195312
    var seed = Math.floor(this.total_ratio*less_than)+offset;
    var val = less_than*util_random.srandom(seed);
    console.log("Random choice", val);
    return Math.floor(val);
};

DNA.prototype.major_choice = function (array, offset) {
    return array[this.major_random(array.length, offset)];
};

DNA.prototype.choice = function (array, offset) {
    return array[this.random(array.length, offset)];
};


DNA.prototype.mutated = function (options) {
    var opts = _.extend({
            always_mutate: true,
            mutation_range: 1,
            seed: false,
            generations: 1
        }, options);

    var i = 0;
    var new_string = this.string;
    var randgen = new util_random.SeededRandom(opts.seed);

    // Apply mutation for each generation
    while (i++ < opts.generations) {
        var index = randgen.randint(this.vals.length);
        console.log("Randomly choosing", index);
        var val = this.vals[index];
        var diff = randgen.randint(opts.mutation_range)-1;

        // never have 0 diff, shift 0 and all positive up one
        if (opts.always_mutate && diff >= 0) {
            diff += 1;
        }

        new_string = new_string.substr(0, index)
                                + (val + diff).toString(BASE)
                                + new_string.substr(index+1);
    }

    return new DNA(new_string);
};

var random_dna_string = function (length, seed) {
    var length = 32 || length;
    var randgen = new util_random.SeededRandom(seed || false);
    var r = [];
    for (var i=0; i<length; i++) {
        r.push(randgen.randint(BASE).toString(BASE));
    }
    return r.join('');
};



module.exports.DNA = DNA;
module.exports.random_dna_string = random_dna_string;
