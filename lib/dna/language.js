var _ = require('underscore');
_.str = require('underscore.string');

var PHONEME_COUNT = 16;

var m_structures = [
    'cv',
    'v',
    'cvc',
    'vc',
    'cvvc',
    'cvy',
    'vy',
    'yv',
    'yvc'
];

var m_types = {
    v: _.str.chars('aeiou'),
    c: _.str.chars('rtpsdhgklvbn'),
    y: _.str.chars('ywcqçzmjx') // exotic / redundant
};

var MIN_MORPHEMES = 20;

var get_morphemes = function (dna) {
    // first get 3 prevalent structures --- first genes are thus really
    // important
    dna.reset();
    var structures = [
        dna.major_choice(m_structures, true),
        dna.major_choice(m_structures, true),
        dna.major_choice(m_structures, true)
    ];
    // Get number of morphenes (also a first gene, also important)
    var morpheme_count = dna.random(16, true) + MIN_MORPHEMES;


    dna.reset();
    // Now create how many digraphs, which will be used with similar frequency
    // as consenants
    /*
    var digraph_count = dna.random(8, true);
    var digraphs = [];

    for (var i=0; i<digraph_count; i++) {
        // and generate the digraphs, which will be only cc
        var digraph = dna.choice(m_types.c, true) + dna.choice(m_types.c, true);
        digraphs.push(digraph);
    }
    */

    // less important latter genes
    var morphemes = [];
    for (var i=0; i<morpheme_count; i++) {
        var structure = dna.choice(structures, true);
        var split = _.str.chars(structure);
        var m = '';
        for (var j in split) {
            m += dna.choice(m_types[split[j]], true);
        }
        morphemes.push(m);
    }
    return morphemes;
};


var Language = function (dna) {
    // build a language from the given DNA
    this.morphemes = get_morphemes(dna);
    this.dna = dna;
};

Language.prototype.get_word = function (index, word_size) {
    this.dna.base_offset = index;
    var variance = Math.floor(word_size / 2);
    if (variance > 0) {
        word_size += this.dna.random(variance, true) - Math.floor(variance/2);
    }

    var result = [];
    for (var i=0; i<word_size; i++) {
        result.push(this.dna.choice(this.morphemes, true));
    }
    return result.join("");
};

module.exports.Language = Language;

