var _ = require('underscore');
_.str = require('underscore.string');

var terrain = require('../map/terrain');

var tiles = {
    free: {
        name: 'Free people',
        ascii: 'f',
        unicode: '263C',
        color: { fg: 'cyan' },
    },

    slave: {
        name: 'Slaves (chattel)',
        ascii: 's',
        unicode: '260D',
        color: { fg: 'red' },
    },

    population: {
        name: 'Population',
        ascii: 'p',
        unicode: '263B',
        color: { fg: 'cyan' },
    },

    bond: {
        name: 'Slaves (bond)',
        ascii: 'b',
        unicode: '260C',
        color: { fg: 'red' },
    },

    serf: {
        name: 'Serf',
        ascii: 'b',
        unicode: '265F',
        color: { fg: 'red' },
    },

    gold: {
        name: 'Gold',
        ascii: 'b',
        unicode: '26C1',
        color: { fg: 'red' },
    },

    asterisk: {
        name: 'Asterisk',
        ascii: "*",
        unicode: '2217',
        color: { fg: 'white' }
    },

    rigidity: {
        name: 'Rigidity',
        ascii: 'r',
        unicode: '211D',
    },

    culture: {
        name: 'Culture',
        ascii: 'c',
        unicode: '266C',
        color: { fg: 'magenta' },
    },

    crossed: {
        name: 'Same as last turn',
        ascii: 'x',
        unicode: '2612',
        color: { fg: 'white' },
    },

    checked: {
        name: 'Checked',
        ascii: 'X',
        unicode: '2611',
        color: { fg: 'white' },
    },

    unchecked: {
        name: 'Unchecked',
        ascii: 'O',
        unicode: '2610',
        color: { fg: 'white' },
    },

    knowledge: {
        name: 'Knowledge',
        ascii: 'k',
        unicode: '2624',
        color: { fg: 'blue' },
    },

    food: {
        name: 'Food',
        ascii: 'f',
        unicode: '2609',
        color: { fg: 'green' },
    },

    danger: {
        name: 'Danger',
        ascii: 'd',
        unicode: '2620',
        color: { fg: 'red' },
    },

    waste: {
        name: 'Waste',
        ascii: 'W',
        unicode: '2297',
        color: { fg: 'red' },
    },

    wheat: {
        name: 'Wheat',
        ascii: 'w',
        unicode: '2226',
        color: { fg: 'white' },
    },

    defense: {
        name: 'Defense',
        ascii: 'D',
        unicode: '22D3',
        color: { fg: 'white' },
    },

    attack: {
        name: 'Attack',
        ascii: 'a',
        unicode: '2694',
        color: { fg: 'white' },
    },

    archery: {
        name: 'Archery',
        ascii: 'A',
        //unicode: '21A1',
        unicode: '2927',
        color: { fg: 'white' },
    },


};

tiles = _.extend({}, tiles, terrain.resources);

var make_char = function (type, options) {
    var s = '', ch='';

    if (options.color && type.color) {
        if (type.color.bg) {
            s += '{'+type.color.bg+'-bg}';
        } else if (options.show_territory && region_player) {
            s += '{'+(player_colors[region_player]||'black')+'-bg}';
        }

        if (type.color.fg) {
            s += '{'+type.color.fg+'-fg}';
        }
    }


    if (options.unicode && type.unicode) {
        ch = String.fromCharCode(parseInt(type.unicode, 16));
    } else {
        ch = type.ascii;
    }

    if (options.strikethrough) {
        ch = strikethrough(ch, options.strikethrough_color || null);
    }

    s += ch;


    if (options.color && type.color) {
        if (type.color.bg) {
            s += '{/'+type.color.bg+'-bg}';
        } else if (options.show_territory && region_player) {
            s += '{/'+(player_colors[region_player]||'black')+'-bg}';
        }

        if (type.color.fg) {
            s += '{/'+type.color.fg+'-fg}';
        }
    }

    return s;
};


var VALUE_TEMPLATE = _.template(
        '<%= bg_start %><%= ch %><%= tiny_space %><%= value %><%= out_of %> <%= bg_end %><%= label %><%=space %>');
var format_value = function (name, val, opts) {
    var opts = opts || { unicode: true, color: true };
    var tile = tiles[name];
    var ch = ' ', label = name;
    if (tile) {
        ch = make_char(tile, opts);
        if (tile.color && tile.color.fg === 'black') {
            tile.color.fg = 'white';
        }
        label = tile.name;
    }

    var s = VALUE_TEMPLATE({
            //label: label,
            value: opts.skip_value ? "" : val,
            tiny_space: opts.skip_value ? "" : " ",
            ch: ch,
            label: opts.skip_label ? "" : label,
            space: opts.skip_space ? "" : "    ",
            out_of: opts.out_of ? "/"+opts.out_of : "",
            bg_start: opts.skip_bg ? "" : "{black-bg}",
            bg_end: opts.skip_bg ? "" : "{/black-bg}",
        });
    return s;
};

var METER_TEMPLATES = [
    _.template('[<%= e %><%= e %>]'),
    _.template(
        '{black-fg}{<%= color %>-bg}<%= e %>{/<%= color %>-bg}{/black-fg}'+
        '{cyan-fg}{black-bg}<%= f %>{/black-bg}{/cyan-fg}')];

var meter = function (value, out_of, opts) {
    var ch_e = _.str.repeat(opts.color ? "-" : "#", value);
    var ch_f = _.str.repeat(opts.color ? " " : "-", out_of-value-1);
    if (opts.unicode && ch_f.length === 0) {
        ch_e = ch_e + "↬ ";
        //↬↶⤺   
    }

    return METER_TEMPLATES[(!!opts.color)*1]({
                e: ch_e, f: ch_f,color: opts.color});
};

var precomputed_years = (function () {
    var r = {};
    var t = 0;

    // canonical stone age
    for (var y=-8000; y<-4000; y += 200) { r[t]=y; t++; }

    // canonical bronze age
    for (var y=-4000; y<-1000; y += 100) { r[t]=y; t++; }

    // canonical iron age
    for (var y=-1000; y<-300; y += 50) { r[t]=y; t++; }

    // canonical classical age
    for (var y=-300; y<300; y += 25) {
        if (y === 0) {
            r[t]=1;
        } else {
            r[t]=y;
        }
        t++;
    }

    // canonical medieval age
    for (var y=300; y<1400; y += 50) { r[t]=y; t++; }

    // canonical colonization age
    for (var y=1400; y<1700; y += 25) { r[t]=y; t++; }

    // canonical industrial era
    for (var y=1700; y<1880; y += 10) { r[t]=y; t++; }

    // canonical war era
    for (var y=1880; y<1940; y += 2) { r[t]=y; t++; }

    // atom age
    for (var y=1940; y<1970; y += 2) { r[t]=y; t++; }

    // information age
    for (var y=1970; y<2000; y += 1) { r[t]=y; t++; }

    return r;
})();

var compute_year = function (turn_no) {
    var year = precomputed_years[turn_no] || "FUTURE";
    if (year < 0) {
        year = _.str.trim(year, '-');
        return year + " B.C.E";
    } else {
        return year + " C.E";
    }
};

//var S_T_CHAR = " ̶";
var S_T_CHAR = String.fromCharCode(parseInt("20E0", 16));
var strikethrough = function (text, color) {
    /*
    var ch = S_T_CHAR;
    if (color) {
        ch = "{"+color+"-fg}"+ch+"{/"+color+"-fg}";
    }
    return _.str.chars(text).join(ch) + ch;
    */
    return '{black-bg}{bold}'+text+'{/bold}{/black-bg}';
};


module.exports.compute_year = compute_year;
module.exports.compute_year = compute_year;

module.exports.tiles = tiles;
module.exports.make_char = make_char;
module.exports.format_value = format_value;
module.exports.meter = meter;

module.exports.strikethrough = strikethrough;

