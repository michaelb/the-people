// a very hairy random number generator
var _ = require('underscore');

var LOW = false;
var HIGH = true;

var difficulties = {
    VERY_HARD: 4,
    HARD: 3,
    NORMAL: 2,
    EASY: 1,
    VERY_EASY: 0
};

// Kind of like a binomial table, d6-ified, and then normalized slightly, and jiggled
var random_table = {};

random_table[difficulties.NORMAL] = {
    '0': [ 0, 0, 0, 0, 0, 0 ],
    '1': [ 0, 0, 0, 1, 1, 1 ],
    '2': [ 0, 0, 1, 1, 2, 2 ],
    '3': [ 0, 1, 1, 2, 2, 3 ],
    '4': [ 1, 1, 2, 2, 3, 3 ],
    '5': [ 1, 2, 2, 3, 3, 4 ],
    '6': [ 1, 2, 3, 3, 4, 5 ],
    '7': [ 1, 2, 3, 4, 5, 6 ],
    '8': [ 2, 3, 3, 4, 5, 6 ],
    '9': [ 2, 3, 4, 5, 6, 7 ],
    '10': [ 2, 3, 4, 5, 6, 8 ],
    '11': [ 3, 4, 4, 6, 7, 8 ],
    '12': [ 3, 4, 5, 6, 8, 9 ],
    '13': [ 3, 4, 5, 7, 8, 10], 
    '14': [ 4, 5, 6, 7, 9, 10], 
    '15': [ 4, 5, 6, 8, 9, 11], 
    '16': [ 4, 6, 7, 8, 10, 12], 
    '17': [ 5, 6, 7, 9, 11, 12], 
    '18': [ 5, 6, 8, 9, 11, 13], 
    '19': [ 5, 7, 8, 10, 12, 14], 
    '20': [ 6, 7, 9, 10, 12, 14], 
};

var _make = function (translation) {
    var n = random_table[difficulties.NORMAL];
    var new_table = {}
    for (var val in n) {
        var row = n[val];
        new_table[val] = translation(val, row);
    }
    return new_table;
};


random_table[difficulties.VERY_HARD] = _make(function (val, row) {
    return [0, Math.floor(row[0]/2), row[0], row[0], row[1], row[2]];
});

random_table[difficulties.HARD] = _make(function (val, row) {
    return [0, row[0], row[0], row[1], row[2], row[3]];
});

random_table[difficulties.EASY] = _make(function (val, row) {
    return [row[2], row[3], row[4], row[5], row[5], val*1 ];
});

random_table[difficulties.VERY_EASY] = _make(function (val, row) {
    return [row[3], row[4], row[5], row[5], val*1, val*1];
});


var get_row = function (base_value, what_i_want, difficulty) {
    var key = (base_value%20)+'';

    if (what_i_want === LOW) {
        // invert difficulty, since we want a low result
        difficulty = difficulties.VERY_HARD - difficulty;
    }

    return random_table[difficulty][key];
};

var base = function (original_value, what_i_want, difficulty) {
    var v = 0;
    if (_.isNaN(original_value) || !_.isNumber(original_value)) {
        console.trace(original_value);
        throw "Bad value passed to base randomizer."
    }

    if (original_value === 0) {
        return 0;
    }

    if (original_value < 0) {
        what_i_want = !what_i_want;
    }

    var base_value = Math.abs(original_value);

    while (base_value > 0) {
        var row = get_row(base_value, what_i_want, difficulty);
        v += row[Math.floor(Math.random()*6)];
        base_value =- 20;
    }


    return v * (original_value < 0 ? -1 : 1);
};

var Randomizer = function (difficulty) {
    // Allows "cheating" random number generation
    difficulty = (typeof difficulty === "undefined") ?
                difficulties.NORMAL : difficulty;
    this.base = function (base_value, what_i_want) {
        return base(base_value, what_i_want, difficulty);
    };
};


var srandom = function (seed) {
    var x = Math.sin(seed) * 10000;
    return x - Math.floor(x);
}

var SeededRandom = function (seed) {
    if (seed === 0) {
        seed += 1;
    }
    this.seed = seed || Math.ceil(Math.random()*10000);
};

SeededRandom.prototype.random = function () {
    this.seed++;
    return srandom(this.seed);
};

SeededRandom.prototype.randint = function (less_than) {
    return Math.floor(this.random() * less_than);
};

SeededRandom.prototype.choice = function (array) {
    return array[this.randint(array.length)];
};


module.exports.srandom = srandom;
module.exports.SeededRandom = SeededRandom;
module.exports.Randomizer = Randomizer;
module.exports.difficulties = difficulties;
module.exports.LOW = LOW;
module.exports.HIGH = HIGH;

