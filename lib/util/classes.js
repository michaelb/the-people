
var _ = require('underscore');
_.str = require('underscore.string');




var CoordDict = function () {
    /*
     * Like a normal dictionary, only with coordinate pairs as indices
     */
    var data = {};
    var coords = {};
    this.set = function (c, val) {
        data[c.x+":"+c.y] = val;
        coords[c.x+":"+c.y] = c;
    };

    this.get = function (c) {
        return data[c.x+":"+c.y];
    };

    this.del = function (c) {
        delete data[c.x+":"+c.y];
    };


    this.setdefault = function (c, val) {
        if (typeof data[c.x+":"+c.y] === "undefined") {
            this.set(c, val);
        }
        return this.get(c);
    };

    this.data = data;
    this.coords = coords;
};

/* * A Set for coordinates pair objects (such as {x: 1, y: 1}, treating coords
 * as immutable) */

var CoordSet = function (iterable) {
    var dict = new CoordDict();

    this.add = function (c) {
        dict.set(c, true);
    };

    this.add_all = function (list) {
        for (var i in list) {
            this.add(list[i]);
        }
    };

    this.remove = function (c) {
        dict.del(c);
    };

    this.has = function (c) {
        return !!(dict.get(c));
    };

    this.list = function (c) {
        return _.values(dict.coords);
    };

    if (iterable) {
        this.add_all(iterable);
    }
};


module.exports.CoordDict = CoordDict;
module.exports.CoordSet  = CoordSet;

