var _ = require('underscore');
_.str = require('underscore.string');

var BaseEntity = require('./base').BaseEntity;

var Regime = function (data) {
    BaseEntity.call(this, 'regime', data.name);
    this.data = data;
};

Regime.prototype = BaseEntity;

Regime.prototype.increment = function () {
    // return True if ready for regime change
    this.data.marker += 1;

    if (this.data.marker >= this.info.turns) {
        this.data.marker = 0;
        return true;
    }
    return false;
};

Regime.prototype.is_next_change = function () {
    return this.data.marker+1 >= this.info.turns;
};

Regime.prototype.max_pop = function (type) {
    // return True if ready for regime change
    return this.info.population[type] || 0;
};

module.exports.Regime = Regime;

