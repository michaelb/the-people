var _ = require('underscore');
_.str = require('underscore.string');

var civinfo = require('../util/civinfo');
var helper = require('../util/helper');
var task_mod = require('./task');
var Regime = require('./task').Regime;
var base = require('./base');


var files = _.clone(base.era_list)
        // what's actually implemented:
        .slice(0, 1);

for (var i in files) {
    var name = files[i];
    var fn = "../tech/eras/"+name;
    base.eras[name] = require(fn);
}



var Tech = function (data, era_name, category, name, wheel_number, cost, path, tech_name) {
    this.data = data;
    this.era_name = era_name;
    this.category = category;
    this.name = name;
    this.tech_name = tech_name;
    this.wheel_number = wheel_number;
    this.cost = cost;
    this.path = path;

    if (this.path !== null) {
        // lets extend with choic
        if (!this.has_choice()) {
            console.error(data);
            throw "Path specified for pathless tech." + name;
        }
        _.extend(this.data, this.data.choice[this.path]);
    }
};

Tech.available = function (era_name, tech_list) {
    // gives technology available to the given era with the given tech list
    var children = [];

    // first add all center techs in unlocked trees
    for (var i in base.era_list){
        var e_name = base.era_list[i];
        var era = base.eras[e_name];
        for (var w_i in era.wheels) {
            var wheel = era.wheels[w_i];
            children.push(e_name + "." + wheel._first);
        }

        if (e_name === era_name) {
            // found our era, stop
            break;
        }
    }


    // then add in edges
    for (var i in tech_list) {
        var name = tech_list[i];
        var tech = get(name);
        children = children.concat(tech.children_names());
    }

    // all children
    children = _.uniq(children);

    // remove already researched tech
    children = _.difference(children, tech_list);

    return children;
};


Tech.prototype.children_names = function () {
    if (!this.data.unlocks) {
        return [];
    }
    var ea = this.era_name;
    var spokes = base.eras[ea].wheels[this.wheel_number][this.data.unlocks];

    var result = [];
    for (var key in spokes) {
        if (key.indexOf('_') !== 0) {
            result.push(ea + "." + key);
        }
    }
    return result;
};


Tech.prototype.full_name = function () {
    return this.era_name + "." + this.name;
};


Tech.prototype.label = function () {
    return _.str.humanize(this.name);
};


Tech.prototype.has_choice = function () {
    return !!this.data.choice;
};

Tech.prototype.blurb = function (opts, path) {
    var info={}, s='';
    if (this.has_choice()) {
        info = this.data.choice[path];
    } else {
        info = this.data;
    }

    if (!info || !info.tasks) {
        console.info(this.data);
        throw "Could not display blurb for path " + path +
                        "    - tech:   " + this.name;
    }

    s += "Allows: ";
    for (var i in info.tasks) {
        var task_name = info.tasks[i];
        s += _.str.titleize(_.str.humanize(task_name)) + '  ';
    }
    s += "\nCost: " + civinfo.format_value("knowledge", this.cost);

    if (info.qualities) {
        // this path affects civ qualities
        s += "\nChange:";
        for (var type in info.qualities) {
            s += " " + civinfo.format_value(type, "+" + info.qualities[type]);
        }
    }

    return s;
};

Tech.prototype.children = function () {
    var names = this.children_names();
    for (var i in names) {
        techs.push(get(names[i]));
    }
    return techs;
};


Tech.prototype.get_tasks = function () {
    var era_name = this.era_name;
    var res = [];
    for (var i in this.data.tasks) {
        var name = this.data.tasks[i];
        if (name.indexOf(".") === -1) {
            name = era_name + "." + name;
        }
        res.push(new task_mod.Task(name));
    }
    return res;
};

var get = function (name) {
    var split = name.split(".");
    var era_name = split[0];
    var tech_split = split[1].split(":");
    var tech = tech_split[0];
    var path = tech_split.length > 1 ?
                parseInt(tech_split[1], 10) : null;
    var era = base.eras[era_name];
    var cats = ['center', 'core', 'left', 'right'];
    for (var i_w in era.wheels) {
        var wheel = era.wheels[i_w];
        for (var i in cats) {
            var c = cats[i];
            if (wheel[c] && wheel[c][tech]) {
                var cost = wheel[c]._cost;
                var info = helper.deepclone(wheel[c][tech]);
                return new Tech(info, era_name, c, tech, i_w, cost, path, tech);
            }
        }
    }

    // couldn't find
    throw "Could not find tech: '" + name + "'";
};



module.exports.Tech = Tech;
module.exports.eras = base.eras;
module.exports.get = get;


// tack on these since other mods expect it here
module.exports.Task = task_mod.Task;
module.exports.shorthand = task_mod.shorthand;
module.exports.TaskList = task_mod.TaskList;
module.exports.era_list = base.era_list;
