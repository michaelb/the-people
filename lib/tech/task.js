var _ = require('underscore');
_.str = require('underscore.string');

var base = require('./base');

var shorthand = {
    "k": "knowledge",
    "c": "culture",
    "f": "food",
    "w": "waste",
    "W": "wheat",
    "g": "game",
    "o": "forage",
    "s": "stone",
    "=": "wood",
    "d": "danger",
    "D": "defense",
    "A": "attack",
    "a": "archery",

    "*": "asterisk",
};

var Task = function (name) {
    var split = name.split(".");
    var era_name = split[0];
    var task_name = split[1];
    var era = base.eras[era_name];

    var info = base.eras[era_name].tasks[task_name];

    if (!info.label) {
        // give it an automatic label
        info.label = _.str.titleize(_.str.humanize(task_name));
    }

    if (!info) {
        throw ("Unknown task: '" + name + "'")
    }

    this.info = info;
    this.name = name;
    this.assignments = {};
    this.forced = {};
    this.assigned = 0;
};

Task.prototype = new base.BaseEntity;

Task.prototype.get_assignment_list = function (type) {
    var assignment_list = [];
    for (var type in this.assignments) {
        var forced_count = this.forced[type] || 0;
        for (var i=0; i<this.assignments[type]; i++) {
            assignment_list.push({
                type: type,
                forced: i<forced_count,
            });
        }
    }
    return assignment_list;
};

Task.prototype.get = function (type) {
    return this.assignments[type] || 0;
};

Task.prototype.assign = function (type, number, is_forced) {
    // calculate how slots are left
    var total = _(this.info.slots)
                    .map(function (a) { return a.length; })
                    .reduce(function (a, b) { return a+b; }, 0) 
                    - this.assigned;

    // number assigned is whatever we can fill up
    var quantity = Math.min(total, number);
    this.assignments[type] = (this.assignments[type]||0) + quantity;
    if (is_forced) {
        this.forced[type] = (this.forced[type]||0) + quantity;
    }
    this.assigned += quantity;

    // return the number left
    return number - quantity;
};

Task.prototype.get_label = function () {
    var label = this.info.label;
    if (this.info.low_priority) {
        label += '   V';
    }
    return label;
};

// used when calculating failed resource requirements, etc
Task.prototype.fully_unemploy = function () {
    // used when calculating failed resource requirements, etc
    this.assigned = 0;
    this.assignments = {};
};

Task.prototype.decrement_employment = function () {
    this.assigned--;
    for (var k in this.assignments) {
        this.assignments[k]--;
        break;
    }
};

Task.prototype.get_totals = function (special_args) {
    var assigned = this.assigned;

    // nothing
    if (assigned === 0) { return {}; }

    var totals = {};
    var special = this.info.special;

    var apply_asterisk = function (ch) {
        var func = special[ch], results=null;

        if (func.length > 2) {
            // requires production phase context info, e.g. scratch or
            // turn
            if (special_args.length > 2) {
                results = func.apply(this, special_args);
            }
        } else {
            // doesn't require many args, should be easy
            results = func.apply(this, special_args);
        }

        if (results) {
            for (var name in results) {
                totals[name] = (totals[name] || 0) + results[name];
            }
        }
    };

    for (var i in this.info.slots) {
        var row = this.info.slots[i];
        for (var j in row) {
            var slot = row[j];
            var chars = _.str.chars(slot);

            assigned--;
            if (assigned < 0) {
                // neg value means we have less than 0 left
                return totals;
            }

            var val = 1;
            for (var k in chars) {
                var ch = chars[k];
                if (ch === '>' || ch === ' ') { 
                    continue;
                } else if (ch === '-') { 
                    // means the rest is negative
                    val = -val;
                    continue;
                } else if (ch.match(/^\d+$/)) {
                    // multiplier
                    val = parseInt(ch, 10);
                    continue;
                } else if (ch === '*') {
                    // asterisk, apply text
                    apply_asterisk(ch);
                    continue;
                }

                var name = shorthand[ch];
                totals[name] = (totals[name] || 0) + val;
            }
        }
    }
    return totals;
};

var TaskList = function (list) {
    this.list = list;
};

TaskList.prototype.get_totals = function (special_args) {
    var totals = {};
    for (var i in this.list) {
        var task = this.list[i];
        // if special args are specified, then apply totals, otherwise
        // skip that part
        var args = [task, this].concat(special_args ? special_args : []);
        _.each(task.get_totals(args), function (val, key) {
            totals[key] = (totals[key] || 0) + val;
        });
    }
    return totals;
};

TaskList.prototype.find = function (name) {
    for (var i in this.list) {
        if (this.list[i].name === name) {
            // found it
            return this.list[i];
        }
    }
    return null;
};


TaskList.prototype.unemploy_by_resource = function (r_name, brutal) {
    for (var i in this.list) {
        var task = this.list[i];
        // TODO: may not be able to always unemploy correctly by resource
        var totals = task.get_totals([task, this]);
        if (totals[r_name] && totals[r_name] < 0) {
            // it requires this resource, has a negative effect on it
            if (brutal) {
                task.fully_unemploy();
            } else {
                task.decrement_employment();
            }
        }
    }
    return totals;
};


TaskList.prototype.phase = function (phase, state, turn, scratch) {
    // "Special text" goes here
    return null;
};

module.exports.Task = Task;
module.exports.TaskList = TaskList;
module.exports.shorthand = shorthand;


