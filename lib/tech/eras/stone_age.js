
var tasks = {

    // CORE
    oral_tradition: {
        slots: [['kk', 'k']],
    },

    the_gathering: {
        slots: [
            ['fff>-o', 'ff'],//          '-o'],
            ['ff   ww>-oo', 'ff   ww'],// '-ooo'],
            ['ff   ww>-oo', 'ff   ww'],// '-ooooo'],
        ],
        default_priority: 10,
    },
    the_hunt: {
        slots: [
            ['fff>-g',     'ff'],//        '-g'],
            ['ff   w>-gg',  'ff   ww'],//   '-ggg'],
            ['ff   dww>-g','ff   dww'],// '-gggg'],
        ],
        default_priority: 10,
    },

    the_catch: {
        slots: [
            ['ff>-F',       'ff'],//        '-g'],
            ['f>-F',        'f'],//   '-ggg'],
            ['f    w>-F',   'f    w'],// '-gggg'],
        ],
        default_priority: 100,
    },

    // SPOKES
    ancestor_guidance: {
        slots: [['*']],
        religious: true,
        special: {
            '*': function (task, task_list, turn, scratch) {
                scratch.adjustments.food_required -= 2;
            }
        }
    },

    spirit_journey: {
        text: "(-1 culture) for each (1 rigidity)",
        slots: [['ccc  *', 'ccc  *']],
        religious: true,
        special: {
            '*': function (task, task_list, turn, scratch) {
                return {
                    culture: -turn.player.data.qualities.rigidity
                }
            }
        }
    },

    coming_of_age: {
        text: "(-2 food) required",
        slots: [['*', '*']],
        religious: true,
        special: {
            '*': function (task, task_list, turn, scratch) {
                scratch.adjustments.food_required -= 2;
            }
        }
    },

    fertility_rites: {
        text: "(+1 free) growth",
        slots: [['*>-f', '*>-f']],
        religious: true,
        special: {
            '*': function (task, task_list, turn, scratch) {
                scratch.adjustments.growth++;
            }
        }
    },

    athletics: {
        slots: [['cc   wd', 'c   wd']],
    },

    expansion: {
        slots: [
                ['w', 'w'],
                ['w', 'w'],
            ],
    },
    night_watch: {
        slots: [['DD   -ddd', 'DD   -ddd']],
    },
    warrior: {
        slots: [['aaaaDD', 'aaaaDD']],
    },
    beadmaking: {
        slots: [['kk', 'k']],
    },
    basketmaking: {
        text: "(-1 waste) per (1 free) on The Gathering",
        slots: [['*', '*']],
        special: {
            '*': function (task, task_list) {
                var task = task_list.find('stone_age.the_gathering');
                return task ? { waste: -task.assigned } : {};
            }
        }
    },
    netmaking: {
        text: "(+1 food) per (1 free) on The Catch",
        slots: [['', '*']],
        special: {
            '*': function (task, task_list) {
                var task = task_list.find('stone_age.the_catch');
                return task ? { food: task.assignments } : {};
            }
        }
    },
};


var wheels = [{
    _first: 'carving',
    center: {
        _cost: 0,
        the_people: {
            label: "The People",
            tasks: ["oral_tradition"],
            unlocks: "core",
        },
    },
    core: {
        _cost: 5,
        carving: {
            label: "Carving",
            tasks: ["the_hunt"],
            regime: ["herb_doctor"],
            unlocks: "right",
        },
        herbalism: {
            label: "Herbalism",
            tasks: ["the_gathering"],
            regime: ["herb_doctor"],
            unlocks: "left",
        },
    },
    left: {
        _cost: 8,
        plant_teachers: {
            label: "Plant Teachers",
            tasks: ["ancestor_guidance"],
        },
        signaling: {
            label: "Signaling",
            tasks: ["expansion"],
        },
        rituals: {
            label: "Rituals",
            choice: [
                {
                    path_label: "Path Into Self",
                    tasks: ["spirit_journey"],
                },
                {
                    path_label: "Path of Elders",
                    qualities: {
                        rigidity: 2,
                    },
                    tasks: ["coming_of_age"],
                },
            ],
        },
        sexuality: {
            label: "Sexuality",
            choice: [
                {
                    path_label: "Gender Roles",
                    qualities: {
                        rigidity: 4,
                    },
                    tasks: ["fertility_rites"],
                },
                {
                    path_label: "Free Competition",
                    tasks: ["athletics"],
                },
            ],
        },
    },
    right: {
        _cost: 5,
        swimming: {
            label: "Swimming",
            tasks: ["the_catch"],
        },
        art: {
            label: "Art",
            tasks: ["beadmaking"],
        },
        stone_tips: {
            label: "Stone Tips",
            choice: [
                {
                    path_label: "Path of Vigilance",
                    tasks: ["night_watch"],
                },
                {
                    path_label: "War Path",
                    qualities: {
                        rigidity: 2,
                    },
                    tasks: ["warrior"],
                },
            ],
        },
        weaving: {
            label: "Weaving",
            choice: [
                {
                    path_label: "Reedworking",
                    tasks: ["basketmaking"],
                },
                {
                    path_label: "Ropemaking",
                    tasks: ["netmaking"],
                },
            ],
        },
    },
}];

var regimes = {
    herb_doctor: {
        turns: 4,
        regions: 2,
        population: {
            free: 6
        },
        effect: {
            production: {
                waste: 2
            }
        },
        change: {
            production: {
                culture: -1,
                knowledge: -1
            }
        }
    },
    hunt_leader: {
        turns: 3,
        regions: 3,
        population: {
            free: 7
        },
        effect: {
            production: {
                danger: 1
            }
        },
        change: {
            production: {
                culture: -1,
                knowledge: -1
            }
        }
    },
};

var abilities = {
    expansion: {
        text: "Gain control of another a region"
    }
};

module.exports.tasks = tasks;
module.exports.abilities = abilities;
module.exports.wheels = wheels;
module.exports.regimes = regimes;

