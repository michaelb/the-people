var _ = require('underscore');
_.str = require('underscore.string');

var eras = {};

var era_list = [
    'stone_age',
    'bronze_age',
    'iron_age',
    'classical_era',
    'medieval_era',
    'age_of_colonization',
    'industrial_era',
    'war_and_revolution',
    'atomic_age',
    'information_age',
    'post_contact',
    'federation_era',
];


var BaseEntity = function (type, name) {
    if (!type) { return; }
    var split = name.split(".");
    var era_name = split[0];
    var task_name = split[1];
    var era = eras[era_name];
    var type_obj = type+"s";

    var info = eras[era_name][type_obj][task_name];

    if (!info.label) {
        // give it an automatic label
        info.label = _.str.titleize(_.str.humanize(task_name));
    }

    if (!info) {
        throw ("Unknown "+ type + ": '" + name + "'")
    }

    this.info = info;
    this.type = type;
    this.name = name;

    this.assignments = {};
    this.assigned = 0;
};

module.exports.BaseEntity = BaseEntity;
module.exports.eras = eras;
module.exports.era_list = era_list;

