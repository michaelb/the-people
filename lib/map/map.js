var _ = require('underscore');
_.str = require('underscore.string');
var util_classes = require('../util/classes');

var mapgen = require('./mapgen');
var terrain = require('./terrain');

var Map = function (data) {
    this.data = data;
};

Map.prototype.get_data = function () {
    return data;
};

Map.prototype.absolute_coords = function (c) {
    // Applies cylindrical world wrapping to given coords
    return {
        x: (c.x < 0 ? this.data.size.height+c.x : c.x)
                    % this.data.size.height,
        y: c.y
    };
};

Map.prototype.get_terrain = function (c) {
    return this.data.terrain[c.x] && 
            this.data.terrain[c.x][c.y];
};

Map.prototype.is_impassible = function (c) {
    return this.is_ocean(c);
};

Map.prototype.is_ocean = function (c) {
    return this.get_terrain(this.absolute_coords(c)) === 'OO';
};

Map.prototype.is_shore = function (c) {
    var res = this.get_terrain(this.absolute_coords(c));
    return res && res.indexOf('o') !== -1;
};




Map.prototype.is_out_of_bounds = function (c) {
    return c.x >= this.data.size.width ||
                c.x < 0 ||
                c.y >= this.data.size.height ||
                c.y < 0;
};

Map.prototype._get_metadata = function (prop, coord, def) {
    if (typeof def === "undefined") {
        def = {};
    }
    return _.clone((this.data[prop][coord.x] &&
            this.data[prop][coord.x][coord.y]) || def);
};

Map.prototype._set_metadata = function (prop, coord, data) {
    // create as needed
    if (!this.data[prop][coord.x]) {
        this.data[prop][coord.x] = {};
    }


    if (!this.data[prop][coord.x][coord.y]) {
        this.data[prop][coord.x][coord.y] = {};
    }

    if (data === null || _.isEmpty(data)) {
        // clear it totally
        delete this.data[prop][coord.x][coord.y];
    }

    // add in extra data
    _.extend(this.data[prop][coord.x][coord.y], data);
};

Map.prototype.get_region = function (coord, def) {
    return this._get_metadata('regions', coord, def);
};

Map.prototype.get_depletion = function (coord, def) {
    return this._get_metadata('depletion', coord, def);
};

Map.prototype.set_region = function (coord, data) {
    return this._set_metadata('regions', coord, data);
};

Map.prototype.set_depletion = function (coord, data) {
    return this._set_metadata('depletion', coord, data);
};


Map.prototype.get_resources = function (c, data) {
    var depletions = (this.data.depletion[c.x]
                        && this.data.depletion[c.x][c.y]) || {};
    var terrain_infos = this.get_terrain_info(c);
    var resources = [];
    for (var i in terrain_infos) {
        var depletion = depletions[i] || 0,
            terrain_info = terrain_infos[i],
            result = {};

        for (var r_name in terrain_info.resources) {
            result[r_name] = terrain_info.resources[r_name] -
                    (depletion[r_name] || 0);
        }
        resources.push(result);
    };

    return resources;
};

/*
var has_left = function (dep) {
    return _(dep).values().reduce(function (a, b) { return a + b; });
};
*/

/* todo need to check if can */
Map.prototype.deplete_evenly = function (c_list, type, value, dry_run) {
    /*
     * depletes evenly over an area
     * NOTE: does not work for negative values (e.g. restoration)
     *
     * Ugh really hairy mess :( :( :( :(
     */

    var last_value = value;
    var should_fully_deplete = false;
    var results = [];
    var depletions = new util_classes.CoordDict();
    while (value > 0) {
        for (var i in c_list) {
            var coord = c_list[i];
            var subregions = this.get_resources(coord);
            var depletion = depletions.setdefault(coord, {});

            var min_val = should_fully_deplete ? 0 : 1;
            for (var i in subregions) {
                var resources = subregions[i];
                if (!resources[type]) {
                    // either depleted or never capable
                    continue;
                }

                var new_depletion = ((depletion[i] || 0) + 1);

                // We would not deplete it too much
                if ((resources[type] - new_depletion) >= min_val) {
                    // deplete once
                    depletion[i] = new_depletion;
                    value -= 1;
                    if (value <= 0) { break; }
                }
            }
            if (value <= 0) { break; }
        }

        if (value === last_value) {
            if (!should_fully_deplete) {
                // could not assign any more depletion without fully depleting
                // this resource, try again with full depletion turned on
                should_fully_deplete = true;
            } else {
                // did not change last iteration, and already tried full
                // depletion, this is a failure
                return value;
            }
        }

        last_value = value;
    }

    if (dry_run) {
        return false;
    }

    // Actually save depletion --- if its unsuccessful we never get here
    for (var i in depletions.coords) {
        var c = depletions.coords[i];
        var results = depletions.data[i];
        var existing_depletion = this.get_depletion(c);
        for (var j in results) {
            if (!existing_depletion[j]) {
                existing_depletion[j] = {};
            }
            existing_depletion[j][type] =
                (existing_depletion[j][type]||0) + results[j];
        }

        this.set_depletion(c, existing_depletion);
    }

    // if we got here we successfully depleted environment, return false for
    // no failures
    return false;
};

Map.prototype.get_terrain_info = function (c) {
    return _.map(_.str.chars(this.get_terrain(c)), function (ch) {
                        return terrain.types[ch];
                    });
};


Map.prototype.replenish = function (super_regrow) {
    /*
     * Loops through all depletions and "increments" them.
     * If they are no longer depleted, marks as replenished (e.g. clears)
     */
    var clear_if_empty = function (obj, value){
        if (!obj[value] || _.isEmpty(obj[value])) {
            delete obj[value];
            if (_.isArray(obj)) {
                // clear out empty values
                _.compact(obj);
            }
        }
    };

    for (var x in this.data.depletion) {
        var depletion_x = this.data.depletion[x];
        for (var y in depletion_x) {
            var depletion_x_y = depletion_x[y];
            var terrain_info = this.get_terrain_info({x: x, y: y});
            for (var i in depletion_x_y) {
                var subregion = depletion_x_y[i];
                var subregion_terrain_info = terrain_info[i];
                for (var r_name in subregion) {
                    var resource_info = terrain.resources[r_name];
                    if (!resource_info.regrow) {
                        continue; // non-regrowable, e.g. stone
                    }

                    if ((subregion[r_name] < subregion_terrain_info.resources[r_name])
                                                        || super_regrow) {
                        // non fully-depleted, will regrow
                        subregion[r_name] -= resource_info.regrow;
                    }

                    if (subregion[r_name] <= 0) {
                        // set to zero, means we should clear this depletion
                        delete subregion[r_name];
                    }
                }

                // clear the subregion if its empty
                clear_if_empty(depletion_x_y, i);
            }
            // clear the y column if empty
            clear_if_empty(depletion_x, y);
        }
        // clear the x column if empty
        clear_if_empty(this.data.depletion, x);
    }
};

Map.prototype.is_suitable_starting_location = function (c) {
    return !(this.is_impassible({x: c.x, y: c.y})
            || this.is_impassible({x: c.x+1, y: c.y})
            || this.is_impassible({x: c.x-1, y: c.y})
            || this.is_impassible({x: c.x+2, y: c.y})
            || this.is_impassible({x: c.x-2, y: c.y}));
};



Map.prototype.choose_starting_location = function (skip) {
    var x_guess = Math.floor(this.data.size.height/2);
    var y_guess = Math.floor(this.data.size.width/2);
    var choice = null;
    var give_up = 1000;

    while (!choice && give_up--) { 
        // Anywhere in map
        //x_guess = Math.floor(Math.random() * this.data.size.width); 
        x_guess = Math.floor(this.data.size.width/4 + 
                            (Math.random() * this.data.size.width/2));

        // Center 1/2 of map
        y_guess = Math.floor(this.data.size.height/4 + 
                            (Math.random() * this.data.size.height/2));

        if (this.is_suitable_starting_location({x: x_guess, y: y_guess})
                    && !_.contains(skip, x_guess+","+y_guess)) {
            // land on either side, sufficiently large, and not within range of
            // previous guesses
            choice = { x: x_guess, y: y_guess };
        }
    }

    if (!choice) {
        throw "Could not find suitable starting location";
    }

    // Add in with a radius of 5 (for 100 map) around starting location a skip
    // list for other peoples
    var RADIUS = Math.floor(this.data.size.height/20);
    var skip_list = [];
    for (var x=choice.x-RADIUS; x<=choice.x+RADIUS; x++) {
        for (var y=choice.y-RADIUS; y<=choice.y+RADIUS; y++) {
            skip_list.push(x+","+y);
        }
    }

    choice.skip_list = skip_list;

    return choice;
};

Map.new_from_options = function (options) {
   return new Map({
            terrain: {},
            size: { width: options.width, height: options.height, },
            regions: {},
            depletion: {},
            original_options: options
        });
};

Map.from_vision = function (global_map, player) {
    //var vision = player.get_tiles_in_vision();
    var vision = player.get_explored_tiles();
    var options = global_map.data.original_options;
    var map = this.new_from_options(options);
 
    for (var i in vision) {
        var c = vision[i];
        if (!map.data.terrain[c.x]) { map.data.terrain[c.x] = {}; }
        map.data.terrain[c.x][c.y] = global_map.get_terrain(c);
        map.set_region(c, global_map.get_region(c));
        map.set_depletion(c, global_map.get_depletion(c));
    }
    return map;
};

Map.new_random = function (opts) {
    var options = _.extend({
            /*height: 75,
            width: 75,*/
            /*height: 20,
            width: 20,*/
            height: 50,
            width: 50,
            land_mass_amount: 2, /* 1 - 5 */
            land_mass_size: 3,   /* 1 - 5 */
            climate: {
                temperature: "temperate",
                polar_caps: true
            },
        }, opts);

    var landmasses = mapgen.generate_landmasses(options);
    var terrain = mapgen.generate_terrain(options, landmasses);
    return new Map({
            terrain: terrain,
            size: { width: options.width, height: options.height, },
            regions: {},
            depletion: {},
            original_options: options
        });
};

module.exports.Map = Map;

