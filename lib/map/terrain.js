
var types = {
    'O': {
        name: 'Ocean',
        ascii: '^',
        unicode: '2652',
        color: {
            fg: 'blue'
        },
        resources: {
            fish: 9,
        },
        impassable: true,
    },
    'o': {
        name: 'Shallows',
        ascii: '~',
        unicode: '2248',
        color: {
            fg: 'cyan'
        },
        resources: {
            fish: 6,
        },
        impassable: false,
    },
    'p': {
        name: 'Plains',
        ascii: '.',
        unicode: '2237',
        color: {
            fg: 'green',
        },
        resources: {
            game: 6,
            forage: 3,
            stone: 1,
        }
    },
    'f': {
        name: 'Forest',
        ascii: 'T',
        unicode: '2660',
        color: {
            fg: 'green',
        },
        resources: {
            game: 3,
            forage: 6,
            wood: 5,
        }
    },

    'j': {
        name: 'Jungle',
        ascii: '!',
        //unicode: '2648',
        unicode: '297E',
        color: {
            fg: 'green',
        },
        properties: {
            danger: 2,
        },
        resources: {
            game: 6,
            forage: 6,
            wood: 5,
        }
    },

    'h': {
        name: 'Hills',
        ascii: 'n',
        unicode: '2601',
        color: {
            fg: 'black',
        },
        resources: {
            game: 2,
            forage: 2,
            stone: 2,
        }
    },

    'd': {
        name: 'Desert',
        ascii: ',',
        unicode: '223B',
        color: {
            fg: 'black',
        },
        resources: {
            game: 2,
            forage: 2
        }
    },

    't': {
        name: 'Tundra',
        ascii: '=',
        unicode: '22CD',
        color: {
            fg: 'black',
            bg: 'white'
        },
        properties: {
            danger: 2,
        },
        resources: {
            game: 4
        }
    },

    '?': {
        name: 'Unknown',
        ascii: ' ',
        color: {
            //fg: 'blue',
            bg: 'black'
        }
    },
};

var resources = {
    game: {
        name: 'Game',
        color: { fg: 'black', },
        ascii: 'g',
        action: 'hunted',
        unicode: '265E',
        regrow: 1,
    },
    forage: {
        name: 'Forage',
        color: { fg: 'black', },
        ascii: 'f',
        action: 'gathered',
        //unicode: '2698',
        unicode: '2619',
        regrow: 1,
    },
    fish: {
        color: { fg: 'black', },
        name: 'Fish',
        action: 'fished',
        ascii: 'f',
        unicode: '221D',
        regrow: 1,
    },
    stone: {
        name: 'Stone',
        color: { fg: 'black', },
        ascii: 's',
        action: 'mined',
        unicode: '2617',
    },
    wood: {
        name: 'Wood',
        color: { fg: 'black', },
        ascii: 'w',
        action: 'chopped',
        unicode: '2630',
    },
};

var resource_values = {
    9: { name: '9', ascii: '9', },
    8: { name: '8', ascii: '8', },
    7: { name: '7', ascii: '7', },
    6: { name: '6', ascii: '6', },
    5: { name: '5', ascii: '5', },
    4: { name: '4', ascii: '4', },
    3: { name: '3', ascii: '3', },
    2: { name: '2', ascii: '2', },
    1: { name: '1', ascii: '1', },
    0: {
        name: 'depleted', ascii: 'x',
        color: { fg: 'red', }, 
    },
};


module.exports.types = types;
module.exports.resources = resources;
module.exports.resource_values = resource_values;

