var _ = require('underscore');
_.str = require('underscore.string');
var helper = require('../util/helper');

/*
 *
 * A few notes:
 *  - 10 regions to a province
 *  - To make provinces the size of an average U.S. State: regions
 *    are 14,000 km2 each, or 118 km on each side. We can round down to
 *    10,000 km2 / 100 km if we want..
 *  - The earth is about 40,000 km in circumference
 *  - Thus, an earth sized planet map should be 338-400 regions along
 *    the center, and  114,000-160,000 regions total!
 *  - if ever in the future regions become irregularly shaped with
 *  N-adjacent regions (as I hope, with N<8 probably, so it could still
 *  be simulated with a grid, although perhaps gradually sparcer as we
 *  get to the poles), then we use some sort of projection algo to be
 *  able to have a proper globe-based map, and then usign
 *
 *
 *  Lots of insight here:
 *   http://www-cs-students.stanford.edu/~amitp/game-programming/polygon-map-generation/
 *
 *  Ultimately:
 *    1. Plot points on sphere
 *    2. Project to grid (e.g. equirectangular)
 *    3. Go through randomly splitting some points into lines creating
 *    new borders (e.g., for pentagons and hexagons, still N<8 edges on every node)
 *        - the underlying tile can be like "6fp" where the "6"
 *        designates shape & connections somehow ("polysquares")
 *        - to keep things simple, there could maybe be only like 50
 *        different combos of shapes and connections (although visually
 *        it can be jiggled, so it looks realistic)
 *    4. Methodically delete polysquares based on distance from equator, as
 *    such:
 *        - The prime meridian should be the same length as the equator
 *        going straight north or straight south, with every tile being
 *        square or better, and the top two tiles touch each other (I
 *        think)
 *        - As we go one way or the other, tiles become increasingly
 *        scarce the closer we get to the poles
 *        - We then may need to "shift" tiles over until the scarcity is
 *        evenly distributed?
 *        - The key is simply still using a polysquare-based region
 *        mechanic in the engine & rules, but displaying it on a global
 *        map.
 *
 *    5. When rendered, randomly perturb all points, to create a
 *    realistic map, and turn all edges into beizer curves to follow
 *    terrain features, so it looks really like "we have this territory
 *    from the dark wood until yonder river"
 *
 */





var climates = {
    temperate: {
        p: 10,
        f: 10,
        h: 5,
        j: 5,
        d: 5
    },
    arid: {
        p: 5,
        f: 5,
        h: 2,
        j: 10,
        d: 10
    },
    humid: {
        p: 5,
        f: 5,
        j: 10,
        h: 5,
        d: 1
    },
    frigid: {
        t: 10,
        p: 5,
        f: 5,
        h: 5,
        j: 1,
        d: 5
    }
};

// 1/10 of the top and bottom are polar caps
var POLAR_CAP_PROPORTION = 0.1;

var Climate = function (climate, height) {
    var freq = climates[climate.temperature];
    var reverse_freq = [];
    var total_chance = 0;
    var POLAR_CAP_SIZE = height*POLAR_CAP_PROPORTION;
    for (var name in freq) {
        var chance = freq[name];
        total_chance += chance;
        reverse_freq.push([chance, name]);
    }


    this.get_land_type = function (x, y) {
        // todo: right now no seed
        var tile = null;
        var polarness = Math.min(height-y, y); // 0 is most polar
        var choices = reverse_freq;
        if (climate.polar_caps && polarness <= POLAR_CAP_SIZE) {
            var p = (POLAR_CAP_SIZE-polarness) / POLAR_CAP_SIZE;
            choices = [[Math.floor(total_chance*p), 't']].concat(reverse_freq);
        }

        var choice = Math.floor(Math.random() * total_chance);
        for (var i in choices) {
            var val = choices[i][0];
            var tile = choices[i][1];
            choice -= val;
            if (choice <= 0) {
                break;
            }
        }

        if (!tile) {
            console.error("Could not select a tile for " +x +"," +y);
        }

        return tile;
    };

    this.get_tile = function (x, y, is_water, near_shore) {
        if (is_water) {
            return "OO";
            if (near_shore) {
                return "oO";
            } else {
                return "OO";
            }
        } else {
            var t1 = this.get_land_type(x, y);
            var t2 = near_shore ? "o" : this.get_land_type(x, y);
            return t1+t2;
        }
    };
};



var generate_terrain = function (options, landmass) {
    var terrain = [];
    var climate = new Climate(options.climate, options.height);
    var x_y = function (x, y) {
        /*
        var res = landmass[helper.mod(x*options.width, options.width)
                            + helper.mod(y, options.height)];
        var res = landmass[helper.mod(x*options.width, options.width)
                            + helper.mod(y, options.height)];
        */
        return landmass[x*options.width + y];
    };

    for (var x=0; x<options.width; x++) {
        terrain.push([]);//[x] = [];
        for (var y=0; y<options.height; y++) {
            var is_water = x_y(x, y);
            var near = [x_y(x+1, y), x_y(x, y+1), x_y(x-1, y), x_y(x, y-1)];
            if (is_water) {
                near = _.map(near, function (a) { return !a; });
            } 
            //[!x_y(x+1, y), !x_y(x, y+1), !x_y(x-1, y), !x_y(x, y+1)]);
            // if land, adjacent to ANY water, if water adjacent to ANY land
            terrain[x][y] = climate.get_tile(x, y, is_water, _.any(near));
        }
    }
    return terrain;
};

var generate_landmasses = function (options) {
    /*
     * Inspired by http://stackoverflow.com/questions/2520131/looking-for-a-good-world-map-generation-algorithm
     * AyexeM's answer in particular
     * http://jsfiddle.net/AyexeM/zMZ9y/
     */
    var tileArray = new Array();
    var probabilityModifier = 0;
    var mapWidth=options.width;
    var mapheight=options.height;

    var landMassAmount=options.land_mass_amount; // scale of 1 to 5
    var landMassSize=options.land_mass_size; // scale of 1 to 5

    for (var i = 0; i < mapWidth*mapheight; i++) {

        var probability = 0;
        var probabilityModifier = 0;

        if (i<(mapWidth*2)||i%mapWidth<2||i%mapWidth>(mapWidth-3)||i>(mapWidth*mapheight)-((mapWidth*2)+1)){

            // make the edges of the map water
            probability=0;
        }
        else {

            probability = 15 + landMassAmount;

            if (i>(mapWidth*2)+2){

                // Conform the tile upwards and to the left to its surroundings 
                var conformity =
                    (tileArray[i-mapWidth-1]==(tileArray[i-(mapWidth*2)-1]))+
                    (tileArray[i-mapWidth-1]==(tileArray[i-mapWidth]))+
                    (tileArray[i-mapWidth-1]==(tileArray[i-1]))+
                    (tileArray[i-mapWidth-1]==(tileArray[i-mapWidth-2]));

                if (conformity<2)
                {
                    tileArray[i-mapWidth-1]=!tileArray[i-mapWidth-1];
                }
            }

            // get the probability of what type of tile this would be based on its surroundings 
            probabilityModifier = (tileArray[i-1]+tileArray[i-mapWidth]+tileArray[i-mapWidth+1])*(19+(landMassSize*1.4));
        }

        rndm=(Math.random()*101);
        tileArray[i]=(rndm<(probability+probabilityModifier));
        
    }

    return tileArray;
};


exports.generate_landmasses = generate_landmasses;
exports.generate_terrain = generate_terrain;
exports.climates = climates;

