
/*
 * Structure a "base" view that all tabs can extend
 */

var blessed = require('blessed');
var _ = require('underscore');
_.str = require('underscore.string');

var BoxBase = function (info, rerender, toggle_me, opts) {
    if (!info) { return; }
    this.info = info;
    this.rerender = rerender;
    this.toggle_me = toggle_me;
    this.opts = opts || { unicode: true, color: true };
    this.label = "unnamed view";
    this.init();
};

BoxBase.prototype.init = function () {
    this.box = blessed.box({
        top: 'center',
        left: 'center',
        width: '95%',
        height: '90%',
        label: this.info.label,
        content: '',
        tags: true,
        scrollable: true,
        padding: 1,
        border: {
            //type: 'line'
            ch: ' ',
            bg: 'blue'
        },
        style: {
            fg: 'white',
            bg: 'none'
        }
    });

    this.box.key(['escape', 'x', this.info.key], this.toggle_me);
};

BaseBox.prototype.setup = function (turn) {
    this.turn = turn;
    this.refresh();
};


module.exports.BoxBase = BoxBase;
