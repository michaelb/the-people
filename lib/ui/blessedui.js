var blessed = require('blessed');
var asciiui = require('./asciiui');
var tasksui = require('./tasksui');
var moveui = require('./moveui');
var deathui = require('./death');
var endturnui = require('./endturnui');
var knowledgeui = require('./knowledgeui');
var actions = require('../core/actions');
var Player = require('../core/player').Player;
var Regime = require('../tech/regime').Regime;
var Map = require('../map/map').Map;
var civinfo = require('../util/civinfo');
var _ = require('underscore');

var TITLE = "THE PEOPLE";
var TITLE_FORMATTED = '{center}{bold}THE PEOPLE{/bold}';
//var UNICODE_CURSOR = String.fromCharCode(parseInt('2610', 16));

var TITLE_ART = [
        TITLE_FORMATTED + (
        "                 *         '            '      ".substr(TITLE.length)),
        "v0.1          / \\\\         ' ____  '       ",
        "             /   \\ \\        /####\\        ",
        " - - -  - - /     \\  \\  - - - - - - - - -  ",
        "           /_______\\/  \\              ~    ",
        "          /=========\\ ' '\\           ~     ",
        "         /===========\\''   \\       ~~      ",
        "        /=============\\ ' '  \\      ~      ",
        "       /===============\\   ''  \\   ~~      ",
        "      /=================\\' ' ' ' \\   ~~~   ",
        "     /===================\\' ' '  ' \\   ~~  ",
        "    /=====================\\' '   ' ' \\ ~~  ",
        "    /=======================\\  '   ' /  ~~~  ",
        "   /=========================\\   ' /    ~~~~ ",
        "  /===========================\\'  /    ~~~~~ ",
        " /=============================\\/    ~~~~~~~~{/center}"
    ].join("\n");


var UI = function (opts) {
    this.opts = opts;

    // Create a screen object.
    this.screen = blessed.screen();

    // for different screens
    this.screens = {};

    // Quit on Escape, q, or Control-C.
    //this.screen.key(['escape', 'q', 'C-c'], _.bind(function (ch, key) {
    this.screen.key(['q', 'C-c'], _.bind(function (ch, key) {
        this.quit();
        opts.on_quit(ch, key);
    }, this));

    this.screen.key(['C-e'], _.bind(function (ch, key) {
        if (this.signal_turn_ready) {
            this.signal_turn_ready();
        }
    }, this));
    /*
    this.screen.key(['escape', 'x'], _.bind(function (ch, key) {
        this.hide_all_screens();
    }, this));
    */
};


UI.prototype.main_menu = function () {
    /*
     * Display game creation options
     */

    this.clear();

    // Create a box perfectly centered horizontally and vertically.
    var box = blessed.box({
        top: 'center',
        left: 'center',
        width: '80%',
        height: '80%',
        content: TITLE_ART,
        tags: true,
        padding: 1,
        border: {
            type: 'line'
        },
        style: {
            fg: 'white',
            bg: 'black',
            border: {
                fg: '#f0f0f0'
            }
        }
    });

    this.button(box, 'N', this.new_game_menu, {
        top: 2,
        right: 5,
        padding: 1,
        content: 'New Game',
    });

    this.button(box, 'Q', this.quit, {
        bottom: 2,
        right: 5,
        content: 'Quit',
    });

    this.screen.append(box);

    // Focus our element.
    box.focus();

    // Render the screen.
    this.render();
};

UI.prototype.clear = function () {
    var c = this.screen.children;
    for (var i in c) {
        this.screen.remove(c[i]);
    }
    this.screen.render();
};

UI.prototype.quit = function () {
    this.opts.on_quit();
};

UI.prototype.new_game_menu = function () {
    this.clear();

    var options = {
        'difficulty': 'normal',
        'world': 'random'
    };


    // Create a box perfectly centered horizontally and vertically.
    var box = blessed.box({
        top: 'center',
        left: 'center',
        width: '50%',
        height: 14,
        content: 'Start a new game\n'+
            '{bold}Difficulty:{/} Normal\n'+
            '{bold}World:{/} 50x50, temperate',
        tags: true,
        padding: 1,
        border: {
            type: 'line'
        },
        style: {
            fg: 'white',
            bg: 'black',
            border: {
                fg: '#f0f0f0'
            }
        }
    });

    this.button(box, 'S', function () {
            this.opts.on_new_game(options);
        }, {
            top: 2,
            right: 5,
            padding: 1,
            background_color: 'green',
            content: 'Start!',
    });

    this.button(box, 'C', function () {
            console.error("not implemented yet.");
        }, {
        top: 7,
        right: 5,
        content: 'Customize',
    });

    this.button(box, 'B', this.main_menu, {
        bottom: 2,
        right: 5,
        content: 'Back',
    });

    this.screen.append(box);
    // Focus our element.
    box.focus();

    // Render the screen.
    this.render();
};


UI.prototype.turn_ui = function (player_id, state, turn_ready) {
    var civ = state.players[player_id];
    var player = new Player(civ);
    var old_action_data = state.actions && state.actions[player_id];
    var turn;
    if (old_action_data) {
        turn = actions.Turn.from_old(player, _.clone(old_action_data));
    } else {
        turn = new actions.Turn(player);
    }
    this._debug_turn = turn;

    if (player.data.is_dead) {
        this.clear();
        box = new deathui.DeathBox(_.bind(this.render, this),
                _.bind(function () {
                    box.box.detach();
                    box.box.hide();
                    this.render();
                    actions_box.focus();
                }, this),
                {});
        this.screen.append(box.box);
        box.setup(turn);
        this.render();
        return;
    }

    var signal_turn_ready = function () {
        turn_ready(turn.to_data());
    };
    this.signal_turn_ready = signal_turn_ready;

    this.clear();
    /****************************
     * Main UI of the game      *
     ****************************/

    var toolbar = blessed.box({
        top: -1,
        left: 0,
        width: '100%',
        height: 3,
        content: 'The People - Stone Age - Year 8000 BC - Pop 30 - 300 gold',
        tags: true,
        padding: 0,
        border: { type: 'line' },
        style: {
            fg: 'white',
        }
    });

    // Create a box perfectly centered horizontally and vertically.
    var map_box = blessed.box({
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        content: '',
        tags: true,
        padding: 0,
        //border: { type: 'line' },
        style: {
            fg: 'white',
            bg: 'none',
            border: {
                fg: 'black',
            }
        }
    });

    var actions_box = blessed.box({
        top: 2,
        right: 0,
        label: 'Actions',
        width: '20%',
        height: '35%',
        content: '',
        tags: true,
        padding: { left: 2, right: 2 },
        border: {
            type: 'line'
        },
        style: {
            fg: 'white',
            bg: 'none',
            border: {
                fg: 'black',
            }
        }
    });

    var messages_box = blessed.box({
        bottom: 0,
        right: 0,
        label: 'Messages',
        width: '20%',
        height: '65%',
        content: '',
        tags: true,
        padding: { left: 2, right: 2 },
        border: {
            type: 'line'
        },
        scrollable: true,
        style: {
            fg: 'white',
            scrollbar: {
                bg: 'black',
            },
            bg: 'none',
            border: {
                fg: 'black',
            }
        }
    });

    if (player.data.messages) {
        var turn_number=null, messages=null;
        for (turn_number in player.data.messages) {
            messages = player.data.messages[turn_number];
        }

        if (!turn_number) {
            turn_number = 'AI';
            messages = [];
        }

        messages_box.setContent("{center}{bold}TURN "+turn_number +"{/bold}{/center}\n" + messages.join("\n"));
    } else {
        messages_box.setContent("{black-fg}No messages.{/black-fg}");
    }

    this.refresh_toolbar(toolbar, player, state, this.opts);

    this.screen.append(map_box);
    this.screen.append(actions_box);
    this.screen.append(messages_box);
    this.screen.append(toolbar);

    // Find default region to cursor around, should be first in list
    var cursor = this.last_cursor || {x: civ.regions[0].x, y: civ.regions[0].y};
    var zoom = 8;

    this.show_resources = this.show_resources || false;
    this.show_borders = true;

    var unicode = true;
    //var unicode = false;


    //this.refresh_infobox(info_box, civ);

    var refresh_map = _.bind(function () {
            this.refresh_mapbox(map_box, state.map, cursor, {
                                zoom: zoom,
                                color: true,
                                unicode: unicode,
                                show_resources: this.show_resources,
                                show_borders: this.show_borders,
                                //cursor: ":"
                                cursor: "|"
                            });
        }, this);

    /* * Cursor controls */
    this.button(actions_box, '+', function () { if (zoom === 4) zoom = 8; if
        (zoom === 1) zoom = 4; refresh_map(); }, { bottom: 1, right: 1,
            padding: 0, content: '+', });

    this.button(actions_box, '-', function () { if (zoom === 4) zoom = 1; if
        (zoom === 8) zoom = 4; refresh_map(); }, { bottom: 1, right: 2,
            padding: 0, content: '-', });

    this.button(actions_box, 'h', function () { cursor.x--; refresh_map(); }, {
        bottom: 1, left: 1, padding: 0, content: 'h', });

    this.button(actions_box, 'l', function () { cursor.x++; refresh_map(); }, {
        bottom: 1, left: 3, padding: 0, content: 'l', });

    this.button(actions_box, 'j', function () { cursor.y++; refresh_map(); }, {
        bottom: 1, left: 2, padding: 0, content: 'j', });

    this.button(actions_box, 'k', function () { cursor.y--; refresh_map(); }, {
        bottom: 2, left: 2, padding: 0, content: 'k', });

    this.button(actions_box, 'g', function () {
        cursor.x = civ.regions[0].x;
        cursor.y = civ.regions[0].y; refresh_map(); }, {
        bottom: 2, left: 1, padding: 0, content: 'g', });

    this.button(actions_box, 'T', function () { this.toggle_screen('tasks', turn, actions_box); }, {
        top: 1, right: 1, background_color: 'black', content: 'Tasks', });

    this.button(actions_box, 'M', function () { this.toggle_screen('move', turn, actions_box); }, {
        top: 2, right: 1, background_color: 'black', content: 'Move', });

    this.button(actions_box, 'A', function () { this.toggle_screen('attack', turn, actions_box); }, {
        top: 3, right: 1, background_color: 'black', content: 'Attack', });

    this.button(actions_box, 'n', function () { this.toggle_screen('knowledge', turn, actions_box); }, {
        top: 4, right: 1, background_color: 'black', content: 'Knowledge', });

    this.button(actions_box, 'D', function () { this.toggle_screen('demographics', turn, actions_box); }, {
        top: 5, right: 1, background_color: 'black',  content: 'Demographics', });

    this.button(actions_box, 'E', function () { this.toggle_screen('end_turn', turn, actions_box, signal_turn_ready); }, {
        top: 6, right: 1, background_color: 'black',  content: 'End turn', });

    /* * Map settings    */

    var resource_button = this.button(actions_box, 'r', function () {
        var s = '[x] {underline}r{/}es';
        this.show_resources = !this.show_resources;
        if (!this.show_resources) {
            s = s.replace('[x]', '[ ]');
        }

        /*if (unicode) {
            s = s.replace('[ ]', String.fromCharCode(parseInt('2610', 16)));
            s = s.replace('[x]', String.fromCharCode(parseInt('2611', 16)));
        }*/

        resource_button.setContent(s);
        refresh_map();
    }, { bottom: 1, left: 6, padding: 0, content: '[ ] res', });


    /* * Mouse interaction, center on click */
    map_box.on('click', _.bind(function (mouse) {
            this.map_clicked(map_box, zoom, cursor, mouse);
            refresh_map();
            actions_box.focus();
        }, this));

    actions_box.focus();

    refresh_map();
    this.render();
};

UI.prototype.toggle_screen = function (what, turn, actions_box, signal_turn_ready) {
    var box;
    if (this.screens[what]) {
        box = this.screens[what];
    } else {
        var classes = {
            tasks: tasksui.TasksBox,
            knowledge: knowledgeui.KnowledgeBox,
            move: moveui.MoveBox,
            regime: moveui.RegimeBox,
            end_turn: endturnui.EndTurnBox,
        };

        var Klass = classes[what]
        if (!Klass) {
            console.error(" -------------- Not implemented yet. --------------");
            return;
        }

        box = new Klass(_.bind(this.render, this),
                _.bind(function () {
                    box.box.detach();
                    box.box.hide();
                    this.render();
                    actions_box.focus();
                }, this),
                {},
                signal_turn_ready);

    }

    this.screen.append(box.box);
    box.setup(turn);
    this.render();
};


UI.prototype.map_clicked = function (map_box, zoom, center, mouse) {
    // calculate relative map x & map y
    var x = (mouse.x / zoom);
    var y = (mouse.y / zoom);
    // adjust from center
    var mid_height = Math.floor(map_box.height / zoom / 2);
    var mid_width = (Math.ceil(map_box.width - map_box.width/10) / zoom)/ 2;
    center.x += Math.floor(x - mid_width);
    center.y += Math.floor(y - mid_height);
};

UI.prototype.refresh_mapbox = function (map_box, map_data, center, options) {
    //var s = '{center}{bold}MAP{/}\n';
    //var s = '';
    //map_box.setContent(s);
    //this.render();
    var map  = new Map(map_data);
    var height = map_box.height - 2;
    var width = Math.ceil(map_box.width - map_box.width/7);
    var s = asciiui.map_render(map, {
                center_x: center.x,
                center_y: center.y
            }, {
                height: height,
                width: width
            }, options);
    map_box.setContent(s);

    //this.refresh_cursor_info(cursor_i


    this.render();
};



var TEMPLATE = _.template(
            '{bold}THE PEOPLE{/bold}  <%= year %> - <%= era_label %>  '+
            '-  <%= population %> <%= regime %> <%= regime_change %> <%= free_pop %>  -  <%= knowledge %><%= culture %>'
        );

var FUZZY_RANGE = 10;
var fuzzy_population = function (player, turn) {
    var pop = player.pop('total', true);
    var reg = player.data.regions[0];
    var seed = Math.abs(reg.x << 16 + reg.y) + turn;
    var val = seed % FUZZY_RANGE;
    return Math.ceil(pop - FUZZY_RANGE/2 + val);
};

UI.prototype.refresh_toolbar = function (box, player, state, opts) {
    var turn = state.turn_number || 0;
    opts = _.extend({ skip_label:true }, opts);
    var population = fuzzy_population(player, turn);
    var regime = player.get_regime();//new Regime(player.data.regime);
    var regime_change = civinfo.meter(regime.data.marker,
                regime.info.turns, _.extend({}, opts, { color: 'cyan' }));
    var s = TEMPLATE({
            era_label: _.str.titleize(
                           _.str.humanize(player.data.era)),
            population: civinfo.format_value(
                    'population', population,
                        _.extend({}, opts, {
                            bg: "none",
                            skip_bg: true,
                            skip_space: true
                        })),
            knowledge: civinfo.format_value(
                    'knowledge', player.get('knowledge'), opts),
            culture: civinfo.format_value(
                    'culture', player.get('culture'), opts),
            regime: regime.info.label,
            regime_change: regime_change,
            free_pop: civinfo.format_value(
                    'free', player.pop('free'),
                    _.extend({}, opts, {
                        out_of: player.max_pop('free'),
                        skip_space: true
                    })),
            year: civinfo.compute_year(turn),
        });
    box.setContent(s);
};

UI.prototype.refresh_action_box = function (action_box, player) {
    //var actions = player.get_available_actions();
    //action_box.setContent(s);
};

UI.prototype.refresh_infobox = function (info_box, civ) {
    var s = '';
    //var s = '{center}{bold}THE PEOPLE{/bold}{/center}\n';

    s += '{left}'

    var total_pop = 0;
    for (var pop_type in civ.population) {
        var population = civ.population[pop_type];
        total_pop += population;
        if (population > 0) {
            s += pop_type + ': ' + population + "\n";
        }
    }
    s += 'pop: ' + population + "\n";
    s += '{/left}'



    /*
            population: { free: 2 },
            attributes: { free: 2 },
            spendables: { free: 2 },
            regions: [{ x: 2, y: 2 }],
            technology: [ 'stone_age.oral_tradition' ]
    */
    info_box.setContent(s);
};


UI.prototype.render = function () {
    this.screen.render();
};


UI.prototype.button = function (p, key, func, info) {
    if (!func) {
        throw "button: function not specified: (p, key, func, info) {";
    }
    info.parent = p;
    info.mouse = true;
    info.keys = true;
    info.shrink = true;
    if (info.padding === 0) { info.padding = 0; } else {
        info.padding = info.padding || { left: 2, right: 2 };
    }
    info.tags = true;
    info.style = {
            bg: info.background_color || 'none',
            focus: { bg: 'red' },
            hover: { bg: 'red' }
        };
    info.content = info.content.replace(key, '{underline}'+key+'{/}');

    var button = blessed.button(info);
    var bound = _.bind(func, this);
    button.key(key.toLowerCase(), bound);
    button.on('click', bound);
    p.key(key.toLowerCase(), bound);

    return button;
}

exports.UI = UI;

