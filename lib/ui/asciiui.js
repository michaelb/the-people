var _ = require('underscore');
_.str = require('underscore.string');

var terrain = require('../map/terrain');
var util_borderstyle = require('../util/borderstyle');

var make_char = function (terrain_type, region_player, options) {
    var s = '';

    if (options.color && terrain_type.color) {
        if (terrain_type.color.bg) {
            s += '{'+terrain_type.color.bg+'-bg}';
        } else if (options.show_territory && region_player) {
            s += '{'+(region.color||'black')+'-bg}';
        }

        if (terrain_type.color.fg) {
            s += '{'+terrain_type.color.fg+'-fg}';
        }
    }

    if (options.unicode && terrain_type.unicode) {
        s += String.fromCharCode(parseInt(terrain_type.unicode, 16));
    } else {
        s += terrain_type.ascii;
    }

    if (options.color) {
        s += '{/}';
    }

    return s;
};


/*   zoom level 4 (maybe the smallest non minimap one?)
*   4 tall
*   8 across
*/

/*   zoom level 8 (default zoom level)
*   8 tall
*   14 across
*/

var templates = {
    1: ['oX'],
    4: [
         ['#ooXXXX#',
          '#oooXXX#',
          '#ooooXX#',
          '%%%%%%%%'].join("\n"),
         ['#ooooXX#',
          '#ooXXXX#',
          '#oooXXX#',
          '%%%%%%%%'].join("\n"),
         ['#oXXXXX#',
          '#ooXXXX#',
          '#oooooo#',
          '%%%%%%%%'].join("\n"),
         ['#XXXXXX#',
          '#oXooXX#',
          '#oooooo#',
          '%%%%%%%%'].join("\n"),
         ['#oXXXXX#',
          '#oooooX#',
          '#ooXXXX#',
          '%%%%%%%%'].join("\n"),
         ['#oXoXXX#',
          '#oooXoX#',
          '#ooXoXX#',
          '%%%%%%%%'].join("\n"),
    ],
    8: [

           ["#ooXXXXXXXXGJ#",
            "#oooooXXXXXHK#",
            "#ooooPXXXXXIL#",
            "#ooooooXXXXXX#",
            "#ADoooooooXXX#",
            "#BEooopoXXXXX#",
            "#CFoooooooXXX#",
            "%%%%%%%%%%%%%%"].join("\n"),
           ["#oooooooooXGJ#",
            "#ooopooooXXHK#",
            "#ooooXXXXXXIL#",
            "#ooXXXXXXXXXX#",
            "#ADooXXXPXXXX#",
            "#BEoooooXXXXX#",
            "#CFoooooooXXX#",
            "%%%%%%%%%%%%%%"].join("\n"),
           ["#oooooooXXXGJ#",
            "#oooXXXXXXXHK#",
            "#oooXXXXXXXIL#",
            "#ooXXXXXXXXXX#",
            "#ADooXXXPXXoo#",
            "#BEoooooXXooo#",
            "#CFoooooooooo#",
            "%%%%%%%%%%%%%%"].join("\n"),
           ["#XXXXXXXXXXGJ#",
            "#oXXPXooXXXHK#",
            "#oXXXXoooXXIL#",
            "#ooXXooooXXXX#",
            "#ADXooopoooXX#",
            "#BEoooooooooo#",
            "#CFoooooooooo#",
            "%%%%%%%%%%%%%%"].join("\n"),
           ["#ooXXXXXXXXGJ#",
            "#ooopoooXXXHK#",
            "#oooooooooXIL#",
            "#ooooXooooXXX#",
            "#ADoXXXooXXXX#",
            "#BEXXXPXXXXXX#",
            "#CFoooooXXXXX#",
            "%%%%%%%%%%%%%%"].join("\n"),
           ["#ooXXXXXXXXGJ#",
            "#oooXooXoXXHK#",
            "#ooXoXoXXXXIL#",
            "#oooooXXooXXX#",
            "#ADoXooXoXoXX#",
            "#BEooXPXXXXXX#",
            "#CFooXooXXXXX#",
            "%%%%%%%%%%%%%%"].join("\n"),
    ]
};


var shore_templates = {
    1: ['oX'],
    4: [
         ['#oooXXX#',
          '#ooooXX#',
          '#oooXXX#',
          '%%%%%%%%'].join("\n"),
         ['#oooXXX#',
          '#ooXXXX#',
          '#XXXXXX#',
          '%%%%%%%%'].join("\n"),
         ['#oooooo#',
          '#oXXXoo#',
          '#XXXXXX#',
          '%%%%%%%%'].join("\n"),
    ],
    8: [

           ["#ooooooXXXXGJ#",
            "#oooooXXXXXHK#",
            "#ooooPXXXXXIL#",
            "#oooooooXXXXX#",
            "#ADoooXXXXXXX#",
            "#BEooopXXXXXX#",
            "#CFoooooXXXXX#",
            "%%%%%%%%%%%%%%"].join("\n"),
           ["#ADoooooXXXGJ#",
            "#BEopoooXXXHK#",
            "#CFooooXXXXIL#",
            "#oooooooXXXXX#",
            "#XXoooXXPXXXX#",
            "#XXXXXXXXXXXX#",
            "#XXXXXXXXXXXX#",
            "%%%%%%%%%%%%%%"].join("\n"),
           ["#ADoooopooooo#",
            "#BEoooooooooo#",
            "#CFooooooXooo#",
            "#ooXXoooXXXXo#",
            "#XXXXXXXXXXGJ#",
            "#XXXPXXXXXXHK#",
            "#XXXXXXXXXXIL#",
            "%%%%%%%%%%%%%%"].join("\n"),
    ]
};



/* * Should cache these...  */
var make_tile = function (terrain_tile, depletion, region, zoom, options,
                                                cursor, seed, shore_border_style) {
    var tiles = _.str.chars(terrain_tile);
    var terrains = [];

    for (var i in tiles) {
        var tile = tiles[i];
        if (!terrain.types[tile]) {
            console.error("Unknown tile", tile);
        }
        terrains.push(terrain.types[tile]);
    }

    if (zoom !== 1 && zoom !== 4 && zoom !== 8) {
        console.error("UNKNOWN ZOOM", zoom);
    }

    var s = '';
    // Pick template based on if its a shore or not
    if (shore_border_style) {
        variations = shore_templates[zoom];
        //s = util_borderstyle.select_and_rotate(variations, shore_border_style);
        s = variations[(Math.floor(seed/2) % variations.length)];
    } else {
        // Get template
        var variations = templates[zoom];
        s = variations[(Math.floor(seed/2) % variations.length)];
    }


    var terrain_o = terrains[seed%2];
    var terrain_x = terrains[(seed+1)%2];
    var depletion_o = false;
    var depletion_x = false;
    if (depletion) {
        var depletion_o = depletion[seed%2] || false;
        var depletion_x = depletion[(seed+1)%2] || false;
    }

    if (!terrain_o || !terrain_x) {
         // undefined
         console.error("Undefined - ", seed, terrain_tile, terrain_o, " - ", terrain_x);
         throw "Undefined - ", seed, terrain_tile, terrain_o, " - ", terrain_x;
    }

    var char_o = make_char(terrain_o, false, options);
    var char_x = make_char(terrain_x, false, options);

    var border_sides = ' ';
    var border_below = ' ';
    if (cursor) {
        // showing a cursor
        border_sides = cursor;
    }

    /* add in borders */
    if (options.show_borders && region) {
        if (options.color) {
            var colorize = function (s) {
                var color = (region.color||'black');
                return '{'+color+'-bg}'+s+'{/'+color+'-bg}';
            }
            border_below = colorize(border_below);
            if (!region.below_only) {
                border_sides = colorize(border_sides);
            }
        } else {
            border_below = '-';
            border_sides = '-';
        }

    }
    s = s.replace(/#/g, border_sides); // clean up / set  border tiles
    s = s.replace(/%/g, border_below);

    if (options.show_resources) {
        // replace resources
        var show_resource = function (t, label, numbers, dep) {
            var i = 0;
            for (var resource in t.resources) {
                if (i>3) { return; }
                var count = t.resources[resource];
                if (dep && dep[resource]) {
                    count = count - dep[resource];
                }
                var resource_info = terrain.resources[resource];
                var value_info = terrain.resource_values[count];
                s = s.replace(label.charAt(i), make_char(resource_info, false, options));
                s = s.replace(numbers.charAt(i), make_char(value_info, false, options));
                i++;
            }
        };

        show_resource(terrain_o, 'ABC', 'DEF', depletion_o);
        show_resource(terrain_x, 'GHI', 'JKL', depletion_x);
    }
    s = s.replace(/[ABCDEF]/g, 'o');
    s = s.replace(/[GHIJKL]/g, 'X');

    s = s.replace(/X/g, char_x);
    s = s.replace(/o/ig, char_o);

    if (region && zoom >= 8 && options.unicode && !region.below_only) {
        // add in people by default 2, later number of people on tile
        var color = (region.color||'black');
        var p_s = '{'+color+'-fg}'+String.fromCharCode(parseInt('2698', 16)) + '{/}';
        s = s.replace(/P/g, p_s);
        s = s.replace(/p/g, p_s);
    } else {
        s = s.replace(/P/g, char_x);
        s = s.replace(/p/g, char_o);
    }

    return s;
};


var plot = function (lines, y, s) {
    var s_lines = s.split("\n");
    for (var i in s_lines) {
        lines[(y*1)+(i*1)].push(s_lines[i]);
    }
};

var ascii_map_render = function (map, viewport, options) {
    var lines = [];
    var y = 0, x = 0;
    var zoom = viewport.zoom;

    // First generate blank lines
    while (y++ < (viewport.height)*zoom) {
        lines.push([]);
    }

    var do_tile = function (viewport_y, map_x, map_y, cursor) {
        var c = {x: map_x, y: map_y};
        var c_below = {x: map_x, y: map_y+1};
        var depletion = map.get_depletion(c, null);
        var resources = map.get_resources(c, null);
        var regions = map.get_region(c, null);
        if (!regions) {
            regions = map.get_region(c_below, null);
            if (regions) {
                regions.below_only = true;
            }
        }

        /*
        var depletion = (map.depletion[map_x] &&
                    map.depletion[map_x][map_y]) || null;

        var regions = (map.regions[map_x] &&
                    map.regions[map_x][map_y]) || null;
        */

        // either get tile or shroud / off map
        var terrain_tile = map.get_terrain(c) || '??';

        var shore_border_style = null;
        if (map.is_shore(c)) {
            // Get border style
            //
            // 789   /-\
            // 456   |^| 
            // 123   \-/
            shore_border_style = util_borderstyle.get(c,
                    function (coords) { return map.is_ocean(coords); });
        }

        /*
        if (map.terrain[map_x] && map.terrain[map_x][map_y]) {
            terrain_tile = map.terrain[map_x][map_y];
        }
        */

        var seed = Math.abs(map_x << 16 + map_y)
                        + map_x + map_y + map_x*map_y;
        /*var tile_graphics = make_tile(terrain_tile,
                        depletion, regions, viewport.zoom,
                        options, cursor, seed);*/
        /*var tile_graphics = make_tile(terrain_tile,
                        resources, regions, viewport.zoom,
                        options, cursor, seed);*/
        var tile_graphics = make_tile(terrain_tile,
                        depletion, regions, viewport.zoom,
                        options, cursor, seed,
                        // shore info
                        shore_border_style
                        );
        plot(lines, viewport_y*zoom, tile_graphics);
    };

    y = 0; x = 0;
    for (y=0; y<viewport.height; y++) {
        var map_y = viewport.center_y - Math.floor(viewport.height/2) + y;
        if (map_y < 0 || map_y > map.data.size.height*zoom) {
            // out the top or off the bottom
        } else {
            x = 0;
            for (x=0; x<viewport.width; x++) {
                var map_x = (viewport.center_x - Math.floor(viewport.width/2)) + x;
                if (map_x < 0 || map_x > map.data.size.width*zoom) {
                    //  off to the left or right
                } else {
                    var cursor = options.cursor && 
                                map_x === viewport.center_x &&
                                map_y === viewport.center_y;
                    do_tile(y, map_x, map_y, cursor && options.cursor);
                }
            }
        }
    }


    // now turn it into a single string
    var lines_2 = [];
    for (var i in lines) {
        lines_2.push(lines[i].join(''));
    }
    return lines_2;
    //return lines_2.join("\n");
};

var map_render = function (map, viewport, size, options) {
    /* wraps around ascii map render and gives a few things */
    viewport.width = Math.ceil(size.width / (options.zoom*2))
    viewport.height = Math.ceil(size.height / options.zoom)
    viewport.zoom = options.zoom;
    var opts = _.extend({
            color: true,
            unicode: false
        }, options);
    var lines = ascii_map_render(map, viewport, options);

    // Clip to viewport width
    /*
    for (var i in lines) {
        excess = (lines[i].length - viewport.width);
        lines[i] = lines[i].substr(Math.ceil(excess/2), viewport.width-1);
    }
    */
    // Clip to viewport height
    //excess = (lines.length - viewport.height);
    //lines.splice(Math.ceil(excess/2), viewport.height-1);

    return lines.join("\n");
}

module.exports.map_render = map_render;



