var blessed = require('blessed');
var technology = require('../tech/technology');
var civinfo = require('../util/civinfo');
var _ = require('underscore');
_.str = require('underscore.string');



var graphics = [
    "                             __,---'     `--.__", 
    "                           ,-'                ; `.", 
    "                          ,'                  `--.`--.", 
    "                         ,'                       `._ `-.", 
    "                         ;                     ;     `-- ;", 
    "                       ,-'-_       _,-~~-.      ,--      `.", 
    "                       ;;   `-,;    ,'~`.__    ,;;;    ;  ;", 
    "                       ;;    ;,'  ,;;      `,  ;;;     `. ;", 
    "                       `:   ,'    `:;     __/  `.;      ; ;", 
    "                        ;~~^.   `.   `---'~~    ;;      ; ;", 
    "                        `,' `.   `.            .;;;     ;'", 
    "                        ,',^. `.  `._    __    `:;     ,'", 
    "                        `-' `--'    ~`--'~~`--.  ~    ,'", 
    "                       /;`-;_ ; ;. /. /   ; ~~`-.     ;", 
    "-._                   ; ;  ; `,;`-;__;---;      `----'", 
    "   `--.__             ``-`-;__;:  ;  ;__;", 
    " ...     `--.__                `-- `-'", 
    "`--.:::...     `--.__                ____", 
    "    `--:::::--.      `--.__    __,--'    `.", 
    "        `--:::`;....       `--'       ___  `.", 
    "            `--`-:::...     __           )  ;", 
    "                  ~`-:::...   `---.      ( ,'", 
    "                      ~`-:::::::::`--.   `-.", 
    "                          ~`-::::::::`.    ;", 
    "                              ~`--:::,'   ,'", 
    "                                   ~~`--'~", 
    ""
].join("\n")

var DeathBox = function (rerender, toggle_me, opts) {
    this.rerender = rerender;
    this.toggle_me = toggle_me;
    this.opts = opts || { unicode: true, color: true };
    this.init();
};



DeathBox.prototype.init = function () {

    var s = 
            "{center}{red-fg}YOUR PEOPLE HAVE PERISHED.{/red-fg}{/center}\n\n" +
            "{center}Legacy: 0{/center}\n\n" +
            "{black-fg}" + graphics + "{/black-fg}";

    // Create a box perfectly centered horizontally and vertically.
    this.box = blessed.box({
        top: 'center',
        left: 'center',
        width: '95%',
        height: '95%',
        label: '  The People Have Perished  ',
        content: s,
        tags: true,
        scrollable: true,
        padding: 1,
        border: {
            //type: 'line'
            ch: ' ',
            bg: 'blue'
        },
        style: {
            fg: 'white',
            bg: 'none'
        }
    });
};



DeathBox.prototype.setup = function (turn) {
    this.turn = turn;
    this.refresh();
};

DeathBox.prototype.refresh = function () {
};

module.exports.DeathBox = DeathBox;

