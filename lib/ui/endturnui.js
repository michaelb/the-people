var blessed = require('blessed');
var actions = require('../core/actions');
var civinfo = require('../util/civinfo');
var _ = require('underscore');
_.str = require('underscore.string');


var EndTurnBox = function (rerender, toggle_me, opts, signal_turn_ready) {
    this.rerender = rerender;
    this.signal_turn_ready = signal_turn_ready;
    this.toggle_me = toggle_me;
    this.opts = opts || { unicode: true, color: true };
    this.init();
};



EndTurnBox.prototype.init = function () {
    // Create a box perfectly centered horizontally and vertically.
    this.box = blessed.box({
        top: 'center',
        left: 'center',
        width: 60,
        height: 10,
        label: '  End Your Turn  ',
        content: "Are you sure you're ready to end your turn?",
        tags: true,
        scrollable: true,
        padding: 1,
        border: {
            type: 'line',
            //ch: ' ',
            bg: 'blue',
            fg: 'black'
        },
        style: {
            fg: 'white',
            bg: 'none'
        }
    });

    var end_turn = _.bind(function () {
            this.toggle_me();
            this.signal_turn_ready();
        }, this);

    this.box.key(['escape', 'x', 'e'], this.toggle_me);
    this.box.key(['enter'], function () { end_turn(); });

    this.end_turn_button = blessed.button({
            bottom: 1,
            left: "center",
            width: 19,
            height: 3,
            padding: 1,
            content: 'End Turn {black-fg}[ENTER]{/black-fg}',
            tags: true,
            style: {
                fg: 'white',
                bg: 'blue',
                focus: { bg: 'red' },
                hover: { bg: 'red' }
            }
        });

    this.cancel_button = blessed.button({
            top: 1,
            right: 1,
            width: 10,
            height: 1,
            padding: 0,
            content: ' Canc{underline}e{/underline}l x',
            tags: true,
            style: {
                fg: 'white',
                bg: 'blue',
                focus: { bg: 'red' },
                hover: { bg: 'red' }
            }
        });


    this.box.append(this.end_turn_button);
    this.box.append(this.cancel_button);
    this.cancel_button.on('click', this.toggle_me);
};



EndTurnBox.prototype.setup = function (turn) {
    this.turn = turn;
    this.refresh();
};

EndTurnBox.prototype.refresh = function () {
    this.box.focus();
    //this.box.setContent('');
    //this.rerender();
};


module.exports.EndTurnBox = EndTurnBox;

