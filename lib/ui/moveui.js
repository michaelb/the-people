var blessed = require('blessed');
var asciiui = require('../ui/asciiui');
var actions = require('../core/actions');
var _ = require('underscore');
_.str = require('underscore.string');

var TASK_WIDTH = 24;


var TOTAL_TEMPLATE = _.template(
        '{black-bg}<%= ch %> <%= value %> {/black-bg} <%= label %>    ');

/*
    this.SELECTION_STRING = this.opts.color ?  '{blue-bg}' + _.str.repeat(" ", TASK_WIDTH-2) + "{/blue-bg}" : _.str.repeat("+", TASK_WIDTH-2);
    this.DESELECTION_STRING = _.str.repeat(" ", TASK_WIDTH-2);
*/

var MoveBox = function (rerender, toggle_me, opts) {
    this.rerender = rerender;
    this.toggle_me = toggle_me;
    this.opts = opts || { unicode: true, color: true };
    this.init();
};



MoveBox.prototype.init = function () {
    // Create a box perfectly centered horizontally and vertically.
    this.box = blessed.box({
        bottom: 4,
        right: 4,
        width: 25,
        height: 11,
        label: '  Move  ',
        content: '',
        tags: true,
        align: "center",
        valign: "middle",
        padding: 3,
        border: {
            //type: 'line'
            ch: ' ',
            bg: 'blue'
        },
        style: {
            fg: 'white',
            bg: 'none'
        }
    });

    this.box.focus();
    this.box.key(['escape', 'x', 'm'], this.toggle_me);
    var set = _.bind(function (d) {
            //var region_id = this.turn.player.data.regions[0];
            var region_id = 0;
            if (!d) {
                this.turn.clear(actions.TYPE.MOVE);
            } else {
                this.turn.set(actions.Action.new_move(region_id, d));
            }
            this.refresh();
            this.rerender();
        }, this);

    this.box.key(['up', 'n', 'k'], function () { set('N'); });
    this.box.key(['down', 's', 'j'], function () { set('S'); });
    this.box.key(['right', 'e', 'l'], function () { set('E'); });
    this.box.key(['left', 'w', 'h'], function () { set('W'); });
    this.box.key(['.', 'space', 'c'], function () { set(null); });
};



var TEMPLATE = [
            '   N   ',
            'W  .  E',
            '   S   '
        ].join("\n");

MoveBox.prototype.setup = function (turn) {
    this.turn = turn;
    this.refresh();
};

MoveBox.prototype.refresh = function () {
    //var region = this.turn.player.data.regions[0];
    var move = this.turn.get(actions.TYPE.MOVE);
    var move_chars = 
        {
            W: "←",
            N: "↑",
            E: "→",
            S: "↓",
            ".": "·",
        } || { /* ascii */
            N: '^',
            E: '>',
            W: '<',
            S: 'v',
            ".": '.',
        };

    s = TEMPLATE;

    for (var d in move_chars) {
        var ch = move_chars[d];
        //if (d === move.direction && _.isEqual(move.region_id, region)) {
        if ((move && d === move.data.direction)
                || (!move && d === ".")
                || (move && d === "." && move.data.direction===null)) {
            s = s.replace(d, "{green-bg}{black-fg}|"+ch+"|{/}");
        } else {
            s = s.replace(d, "{white-bg}{black-fg} "+ch+" {/}");
        }
    }

    this.box.setContent(s);
    this.box.render();
};


module.exports.MoveBox = MoveBox;

