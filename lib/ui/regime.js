
var blessed = require('blessed');
var technology = require('../tech/technology');
var actions = require('../core/actions');
var civinfo = require('../util/civinfo');
var _ = require('underscore');
_.str = require('underscore.string');




var BoxBase = require('./viewbase').BoxBase;

var RegimeBox = function (rerender, toggle_me, opts) {
    var info = {
        label: 'Regime',
    };
    BoxBase.call(this, info, rerender, toggle_me, opts);
};



RegimeBox.prototype.refresh = function () {
    this.box.append(this.eras_box);
    this.cursor = 0;
    this.choice = 0;
    this.available = [];
};



module.exports.RegimeBox = RegimeBox;

