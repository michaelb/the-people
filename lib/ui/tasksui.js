var blessed = require('blessed');
var asciiui = require('./asciiui');
var civinfo = require('../util/civinfo');
var actions = require('../core/actions');
var technology = require('../tech/technology');
var _ = require('underscore');
_.str = require('underscore.string');

var TASK_WIDTH = 24;


var TOTAL_TEMPLATE = _.template(
        '{black-bg}<%= ch %> <%= value %> {/black-bg} <%= label %>    ');

/*
    this.SELECTION_STRING = this.opts.color ?  '{blue-bg}' + _.str.repeat(" ", TASK_WIDTH-2) + "{/blue-bg}" : _.str.repeat("+", TASK_WIDTH-2);
    this.DESELECTION_STRING = _.str.repeat(" ", TASK_WIDTH-2);
*/

var TasksBox = function (rerender, toggle_me, opts) {
    this.rerender = rerender;
    this.toggle_me = toggle_me;
    this.opts = opts || { unicode: true, color: true };
    this.init();
};

var true_length = function (val) {
    return val.replace(/\{[^\}]+\}/g, '').length;
};



TasksBox.prototype.init = function () {
    // Create a box perfectly centered horizontally and vertically.
    this.box = blessed.box({
        top: 'center',
        left: 'center',
        width: '95%',
        height: '90%',
        label: '  Tasks  ',
        content: '',
        tags: true,
        padding: 1,
        border: {
            //type: 'line'
            ch: ' ',
            bg: 'blue'
        },
        style: {
            fg: 'white',
            bg: 'none'
        }
    });

    var back_button = blessed.button({
            top: 1,
            right: 1,
            width: 9,
            height: 3,
            content: 'Back  {underline}x{/underline}',
            padding: 1,
            style: {
                fg: 'white',
                bg: 'black',
            }

        });

    this.box.append(back_button);
};

TasksBox.prototype.setup = function (turn) {

    this.turn = turn;
    this.player = turn.player;
    var tasks = this.turn.get_task_list();
    var COUNT = tasks.list.length;
    var NUMBER_PER_PAGE = this.number_per_page();

    var cursor = 0;
    var selected = null;

    var refresh = _.bind(function(opts) {
            this.refresh(_.extend({
                    cursor: cursor,
                    selected: selected,
                    tasks: tasks
                }, opts));
            this.rerender();
        }, this);

    var move_cursor = _.bind(function (target) {
            cursor = target % COUNT;
            if (cursor < 0) { cursor = COUNT + cursor; }
            refresh();
        }, this);


    this.box.key(['l', 'right'], function () { move_cursor(cursor+1); });
    this.box.key(['h', 'left'],  function () { move_cursor(cursor-1); });
    this.box.key(['>'], function () { move_cursor(cursor+NUMBER_PER_PAGE); });
    this.box.key(['<'], function () { move_cursor(cursor-NUMBER_PER_PAGE); });


    var assign_worker = _.bind(function (c, type, amount) {
            var a = tasks.list[c];
            var type = type || 'free';
            if (typeof amount === 'undefined') { amount = 1; }

            var result = this.turn.get_free_assigned() + amount;
            if (result > this.turn.player.get_assignments()) {
                //console.error("Already reached max free people assignments...");
                refresh({ warn_assignments: true });
                return;
            }
            this.turn.assign_workers(a.name, type, amount);
            tasks = this.turn.get_task_list();
            refresh();
        }, this);


    var swap = _.bind(function (c, s) {
            var a = tasks.list[c];
            var b = tasks.list[s];
            this.turn.swap_task_priority(a.name, b.name);
            tasks = this.turn.get_task_list();
            refresh();
        }, this);

    var clear_stuff = _.bind(function (also_priority) {
            this.turn.clear(actions.TYPE.ASSIGNMENT);
            if (also_priority) {
                this.turn.clear(actions.TYPE.PRIORITY);
            }
            tasks = this.turn.get_task_list();
            refresh();
        }, this);

    this.box.key(['enter', 'space'], function () {
            if (selected !== null) {
                if (cursor !== selected) {
                    swap(cursor, selected);
                    //this.swap_selection(selected, this.cursor);
                }
                // turn off selected
                selected = null;
            } else {
                // make selected
                selected = cursor;
            }
            refresh();
        });


    this.box.key(['k', 'up'], function () {
            // move any worker to target
            assign_worker(cursor, null, 1);
        });

    this.box.key(['j', 'down'], function () {
            assign_worker(cursor, null, -1);
        });

    this.box.key(['c'], function () { clear_stuff(); });
    this.box.key(['x'], function () { clear_stuff(true); });

    this.box.key(['s'], function () { assign_worker(cursor, 'slave'); });
    this.box.key(['f'], function () { assign_worker(cursor, 'free'); });
    this.box.key(['b'], function () { assign_worker(cursor, 'bond'); });
    this.box.key(['p'], function () { assign_worker(cursor, 'patrician'); });
    this.box.key(['n'], function () { assign_worker(cursor, 'noble'); });

    this.box.key(['C-s'], function () { assign_worker(cursor, 'slave', -1); });
    this.box.key(['C-f'], function () { assign_worker(cursor, 'free', -1); });
    this.box.key(['C-b'], function () { assign_worker(cursor, 'bond', -1); });
    this.box.key(['C-p'], function () { assign_worker(cursor, 'patrician', -1); });
    this.box.key(['C-n'], function () { assign_worker(cursor, 'noble', -1); });


    /*
    this.box.key(['k', 'up'], _.bind(function () {
            if (this.taskboxes[no-1]) {
                this.swap_selection(this.taskboxes[no-1]);
                this.refresh({selected: no-1});
                this.rerender();
            }
        }, this));

    this.box.key(['j', 'down'], _.bind(function () {
            if (this.taskboxes[no+1]) {
                this.swap_selection(this.taskboxes[no+1]);
                this.refresh({selected: no+1});
                this.rerender();
            }
        }, this));
    */

    this.box.focus();

    this.box.key(['escape', 'b', 't'], _.bind(function () {
            this.toggle_me();
        }, this));

    refresh();
};


var plot = function (lines, y, s, x) {
    var s_lines = s.split("\n");
    for (var i in s_lines) {
        var y_i = (y*1)+(i*1);
        if (!lines[y_i]) {
            lines[y_i] = [];
        }

        if (x) {
            var len = true_length(lines[y_i].join(""));
            if (len < x) {
                lines[y_i].push(_.str.repeat(" ", x-len));
            }
        }

        lines[y_i].push(s_lines[i]);
    }
};


var combine = function (lines) {
    return _.map(lines, function (a) { return a.join(''); }).join("\n");
};

var repeat = function (val, count) {
    var result = [];
    for (var i=0; i<count; i++) {
        result.push(val);
    }
    return result;
};


TasksBox.prototype.number_per_page = function () {
    return Math.floor(this.box.width / TASK_WIDTH);
    return Math.min(3, Math.floor(this.box.width / TASK_WIDTH));
};


TasksBox.prototype.refresh = function (opts) {
    opts = opts || {};

    /////////////////////////////////////////////
    ///// FIRST WE REFRESH ALL THE BOXES!
    /////////////////////////////////////////////

    var tasks = opts.tasks;

    var lines = [];
    var PAGE_TEMPLATE = "Page ";

    // calc page stuff
    var number_per_page = this.number_per_page();
    var number_of_pages = Math.ceil(tasks.list.length / number_per_page);
    var current_page = Math.floor((opts.cursor||0) / number_per_page);
    var PAGE_LABEL_WIDTH = Math.floor(TASK_WIDTH / 2);
    if ((number_of_pages+1)*PAGE_LABEL_WIDTH > this.box.width) {
        PAGE_TEMPLATE = "";
        PAGE_LABEL_WIDTH = 2;
    }

    var start = current_page*number_per_page;
    var finish = Math.min((current_page+1)*number_per_page, tasks.list.length);

    if (number_of_pages > 1) {
        /* First pages slots */
        for (var i=0; i<number_of_pages; i++) {
            var page_label = _.str.center(PAGE_TEMPLATE + (i+1), PAGE_LABEL_WIDTH);
            if (current_page === i) {
                page_label = "{blue-bg}{bold}"+page_label+"{/bold}{/blue-bg}";
            } else {
                page_label = "{black-bg}"+page_label+"{/black-bg}";
            }

            plot(lines, 0, page_label);
        }

        plot(lines, 1, "");
    }

    /* Then make priority slots */
    for (var i=start; i<finish; i++) {
        plot(lines, 2, _.str.center(i+1, TASK_WIDTH));
    }

    for (var i=start; i<finish; i++) {
        var task = tasks.list[i];
        var taskbox = new TaskBox(task);

        if (i === opts.cursor) {
            taskbox.is_cursor = true;
        }

        if (i === opts.selected) {
            taskbox.is_selected = true;
        }

        var stuff = taskbox.render();

        plot(lines, 3, stuff.text, (i-start)*TASK_WIDTH);
    };

    /////////////////////////////////////////////
    ///// NOW WE CALCULATE TOTALS
    /////////////////////////////////////////////

    var dummy_scratch = { adjustments: {} };
    var totals = tasks.get_totals([this.turn, dummy_scratch]);
    var s = "{bold}TOTAL PRODUCTION:{/bold} ";
    for (var name in totals) {
        var val = totals[name];
        s += civinfo.format_value(name, val);
    }
    s += "\n";
    s += "{bold}Population:{/bold} ";
    s += 
        (opts.warn_assignments ?  "{black-bg}{red-fg}" : "")
        + this.turn.get_free_assigned()+"/"+this.turn.player.get_assignments()
        + (opts.warn_assignments ?  "{/red-fg}{/black-bg}" : "")
        + " Assignments";

    // add in the actual lines
    s += "\n\n\n";
    s += combine(lines);

    if (current_page === 0) {
        s += "\n\n";
        s += "{black-fg}← →/hl to select | enter/space to swap | ↑↓/jk for forced assignments{/black-fg}";
    }

    this.box.setContent(s);
};

var TEMPLATE = [
            '╭━┳━━━━━╮EF',
            '┃W┃42013┃AB',
            '╰━┫97568┃CD',
            '  ╰━━━━━╯GH'].join("\n");

var TASK_TEMPLATE = [ '╒═╕',
                      '│ │',
                      '└─┘'];

var TASK_TEMPLATE_SELECTED =
                    [ '╱┅╲',
                      '┇ ┋',
                      '╲┅╱'];


var TASK_TEMPLATE_CURSOR =
                    [ '╭┈╮',
                      '┊ ┊',
                      '╰┅╯'];


var ASCII_TEMPLATE = [
            '.-.-----.EF',
            '|W|42013|AB',
            "'-|97568|CD",
            "  '-----'GH"].join("\n");

var TaskBox = function (task) {
    this.info = task.info;
    this.task = task;
    this.is_cursor = false;
    this.is_selected = false;
};

TaskBox.prototype.render = function () {
    var lines = [];
    var stuff = this.render_inner();
    var label = this.task.get_label();
    var height = stuff.height+3;
    /*var t = this.is_cursor ? TASK_TEMPLATE_CURSOR :
            (this.is_selected ? TASK_TEMPLATE_SELECTED : TASK_TEMPLATE);*/
    var t = this.is_selected ? TASK_TEMPLATE_SELECTED : 
                (this.is_cursor ? TASK_TEMPLATE_CURSOR : TASK_TEMPLATE);

    var wrap_color = this.is_cursor ?
        function (s) { return "{blue-fg}" + s + "{/blue-fg}"; } :
        (this.is_selected ?  function (s) { return "{green-fg}" + s + "{/green-fg}"; }
         : function (s) { return "{black-fg}" + s + "{/black-fg}"; });

    // plot titlebar
    plot(lines, 0, 
                wrap_color(t[0].charAt(0)+t[0].charAt(1))+
                label+
                wrap_color(
                    _.str.repeat(t[0].charAt(1), stuff.width-label.length-3)+
                    t[0].charAt(2))
            );

    var trim = function (s) {
        return _.str.trim(_.str.repeat(wrap_color((s))+"\n",
                                        stuff.height), " \n");
    };

    // plot left side bar
    plot(lines, 1, trim(t[1].charAt(0)));

    // plot inner rendered
    plot(lines, 1, stuff.text);

    // plot right side bar
    plot(lines, 1, trim(t[1].charAt(2)), TASK_WIDTH-3);
    //plot(lines, 1, _.str.trim(_.str.repeat(wrap_color((t[1].charAt(2))+"\n"), stuff.height)));


    // plot bottom bar
    lines.push([wrap_color(_.str.trim(t[2].charAt(0)+
                _.str.repeat(t[2].charAt(1), stuff.width-2)+
                t[2].charAt(2), "\n "))]);

    return {
            height: height,
            width: stuff.width+4,
            //text:  _.str.trim(combine(lines)),
            //text:  _.str.trim(combine(lines)),
            text:  combine(lines)
        };

};

TaskBox.prototype.render_text = function (text, opts) {
    if (opts.unicode && opts.color) {
        var t = text.replace(/\([^)]+\)/g, function (match) {
                var m = _.str.trim(match, '( )');
                var split = m.split(' ');
                var o_2 = {skip_label: true, skip_space: true};
                var val = split[0];
                var type  = split[1];
                if (val === '1') {
                    o_2.skip_value = true;
                }
                return civinfo.format_value(type, val, _.extend({}, opts, o_2));
            });

        return "*" + t;
    } else {
        return text;
    }
};

TaskBox.prototype.render_inner = function (opts) {
    var lines = [];
    var zoom = 4;
    var row_count = this.info.slots.length;
    var opts = opts || { unicode: true, color: true };

    var ass_list = this.task.get_assignment_list();

    var y=0;

    // First generate blank lines
    //while (y++ < (row_count*zoom + this.info.text)) { lines.push([]); }

    if (this.info.text) {
        // add center to top
        var text = this.render_text(this.info.text, opts);
        var p_length = TASK_WIDTH-true_length(text)-2;
        var padding;
        if (p_length < 0) {
            padding = "…";
            text = text.substr(0, text.length+p_length-1);
        } else {
            padding = _.str.repeat(" ", p_length);
        }

        if (opts.color) { 
            text = "{white-fg}" + text + "{/white-fg}";
        }
        lines.push([text +padding +"\n"]);
    }

    for (var i in this.info.slots) {
        var row = this.info.slots[i];
        for (var j in row) {
            var slot = row[j];
            var s = TEMPLATE;//.replace("w", " ");
            var split = slot.split('>'); // everything to the right should be formatted outside of hte box
            var chars = _.str.chars(split[0]);
            var negative = false;
            //var alternate = 0;

            for (var k in chars) {
                var ch = chars[k];
                if (ch === '-') {
                    // resource depletion
                    negative = true;
                    alternate = true;
                    continue;
                }
                var name = technology.shorthand[ch];
                var tile = civinfo.tiles[name];
                var o_2 = _.extend({}, opts);
                if (negative) {
                    //if (opts.unicode && (alternate===0)) {
                    if (opts.unicode) {
                        o_2.strikethrough = true;
                        //if (opts.color) { o_2.strikethrough_color = 'white'; }
                    }
                    //alternate = (alternate + 1) % 3;
                }

                if (tile) {
                    ch = civinfo.make_char(tile, o_2);
                }

                var re = new RegExp(((k-(negative?1:0))+""), "g");
                s = s.replace(re, ch);
            }

            var assignment_type = ass_list.length > 0 ? ass_list.shift() : false;

            // Now we add in depletion, if the next one is a depletion
            if (split.length>1) {
                var depletion = _.str.trim(split[1], '-');
                var d_chars = _.str.chars(depletion);
                for (var k in d_chars) {
                    var ch = d_chars[k];
                    var name = technology.shorthand[ch];
                    var tile = _.clone(civinfo.tiles[name]);

                    if (tile) {
                        if (assignment_type) {
                            tile.color = _.clone(tile.color);
                            _.extend(tile.color, { 'fg': 'white' });
                        }
                        ch = civinfo.make_char(tile, opts);
                    }
                    var re = new RegExp("ABCDEFGH".charAt(k), "g");
                    s = s.replace(re, ch);
                }
            }
            s = s.replace(/\d/g, ' ');
            s = s.replace(/[ABCDEFGH]/g, ' ');

            var worker = ' ';
            if (assignment_type) {
                s = '{white-fg}' + s + '{/white-fg}';
                s = s.replace(/\n/g, '{/white-fg}\n{white-fg}');
                var tile = civinfo.tiles[assignment_type.type];
                worker = civinfo.make_char(tile, opts);
                if (assignment_type.forced) {
                    worker = "{black-bg}{bold}" + worker + "{/bold}{/black-bg}";
                }
            } else {
                s = '{black-fg}' + s + '{/black-fg}';
                s = s.replace(/\n/g, '{/black-fg}\n{black-fg}');
            }
            s = s.replace(/W/g, worker);

            plot(lines, i*zoom, s);
        }
    }

    return {
            height: lines.length + (this.info.text ? 1 : 0),
            width: TASK_WIDTH,
            text:  combine(lines),
        };

    var MAX_WIDTH = TASK_WIDTH;

    var lines_2 = [];
    var width = -1;
    for (var i in lines) {
        var l = lines[i].join('');
        lines_2.push(l);
    }

    var height = lines_2.length;
    lines_2.push();
    return {
            height: height,
            //width:  width,
            width: MAX_WIDTH,
            text:  lines_2.join("\n"),
        };
};


module.exports.TasksBox = TasksBox;
