var blessed = require('blessed');
var technology = require('../tech/technology');
var actions = require('../core/actions');
var civinfo = require('../util/civinfo');
var _ = require('underscore');
_.str = require('underscore.string');



var WHEEL_TEMPLATE = [
    "        \u2572           \u2502           \u2571         ", 
    "         \u2572 22222222 \u2502 33333333 \u2571          ", 
    " '.       \u2572         \u2502         \u2571        .' ", 
    "   '.       \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u253c\u2500"+
    "\u2500\u2500\u2500\u2500\u2500\u2500\u2500        .'   ", 
    " 66666666 \u2571         \u2502         \u2572 77777777  ", 
    "       '.\u2571          \u2502          \u2572 .'       ", 
    "        \u2571           \u2502           \u2572         ", 
    "       \u2502        \u250f\u2501\u2501\u2501\u2537\u2501\u2501\u2501"+
    "\u2513        \u2502        ", 
    "_______\u2502 111111 \u2503 CCCCC \u2503 000000 \u2502_______ ", 
    "       \u2502        \u2503       \u2503        \u2502        ", 
    "       \u2502        \u2517\u2501\u2501\u2501\u252f\u2501\u2501\u2501"+
    "\u251b        \u2502        ", 
    "        \u2572           \u2502           \u2571         ", 
    "       .'\u2572          \u2502          \u2571 '.       ", 
    " 88888888 \u2572         \u2502         \u2571 99999999  ", 
    "   .'       \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u253c\u2500"+
    "\u2500\u2500\u2500\u2500\u2500\u2500\u2500        '.   ", 
    " .'       \u2571         \u2502         \u2572        '. ", 
    "         \u2571 44444444 \u2502 55555555 \u2572          ", 
    "        \u2571           \u2502           \u2572         "
].join("\n")



var KnowledgeBox = function (rerender, toggle_me, opts) {
    this.rerender = rerender;
    this.toggle_me = toggle_me;
    this.opts = opts || { unicode: true, color: true };
    this.init();
};



KnowledgeBox.prototype.init = function () {
    // Create a box perfectly centered horizontally and vertically.
    this.box = blessed.box({
        top: 'center',
        left: 'center',
        width: '95%',
        height: '90%',
        label: '  The Wheel of Knowledge  ',
        content: '',
        tags: true,
        scrollable: true,
        padding: 1,
        border: {
            //type: 'line'
            ch: ' ',
            bg: 'blue'
        },
        style: {
            fg: 'white',
            bg: 'none'
        }
    });

    this.box.key(['escape', 'x', 'n'], this.toggle_me);
    var set = _.bind(function (i, choice) {
            var old = this.cursor;
            var old_choice = this.choice;
            this.choice = choice ? 1 : 0; // cast down to "1" or "0"
            this.cursor = Math.max(Math.min(this.available.length-1, i), 0);
            if (old !== this.cursor || old_choice !== this.choice) {
                this.refresh();
                this.rerender();
            }
        }, this);

    var move = _.bind(function (d) {
            var tech = this.available[this.cursor] &&
                        technology.get(this.available[this.cursor]);
            if (d === 'N') {
                if (tech && tech.has_choice() && (this.choice === 1)) {
                    // choice = 0
                    set(this.cursor, 0);
                } else {
                    set(this.cursor-1, 1);
                }
            } else if (d === 'S') {
                if (tech && tech.has_choice() && (this.choice === 0)) {
                    // choice = 0
                    set(this.cursor, 1);
                } else {
                    set(this.cursor+1, 0);
                }
            } else {
                // presently can't select others
                this.refresh();
                this.rerender();
            }
        }, this);

    var try_purchase = _.bind(function () {
            var knowledge = this.turn.player.get('knowledge');
            var tech_name = this.available[this.cursor];
            var choice = this.choice;
            var tech = technology.get(tech_name);
            if (tech.cost > knowledge) {
                console.error("Not enough knowledge to purchase. Have:",
                        knowledge, " Required:", tech.cost);
            } else {
                // set to purchase
                this.turn.set_knowledge(tech_name, choice);
            }
            // presently can't select others
            this.refresh();
            this.rerender();
        }, this);

    this.box.key('1', function () { set(0); });
    this.box.key('2', function () { set(1); });
    this.box.key('3', function () { set(2); });
    this.box.key('4', function () { set(3); });
    this.box.key('5', function () { set(4); });
    this.box.key('6', function () { set(5); });
    this.box.key('7', function () { set(6); });
    this.box.key('8', function () { set(7); });
    this.box.key('9', function () { set(8); });

    this.box.key(['up',    'k'], function () { move('N'); });
    this.box.key(['down',  'j'], function () { move('S'); });
    this.box.key(['right', 'l'], function () { move('E'); });
    this.box.key(['left',  'h'], function () { move('W'); });
    this.box.key(['enter', 'p'], function () { try_purchase(); });

    this.wheel_box = blessed.box({
            top: 2,
            right: 2,
            width: 50,
            height: 22,
            content: '',
            tags: true,
            padding: 1,
            border: {
                type: 'line',
                fg: 'green'
            },
            style: {
                fg: 'white',
                bg: 'none'
            }
        });

    this.box.append(this.wheel_box);

    this.purchase_button = blessed.button({
            top: 2,
            right: 42,
            width: 11,
            height: 3,
            padding: 1,
            content: '{underline}P{/underline}urchase',
            tags: true,
            style: {
                fg: 'white',
                bg: 'green',
                focus: { bg: 'red' },
                hover: { bg: 'red' }
            }
        });


    this.box.append(this.purchase_button);

    this.eras_box = blessed.box({
            bottom: 1,
            right: 1,
            width: "100%",
            height: 3,
            label: 'ERAS',
            content: 'lol',
            tags: true,
            padding: 0,
            scrollable: true,
            border: {
                ch: '-',
                fg: 'black'
            },
            style: {
                fg: 'white',
                bg: 'none'
            }
        });

    this.box.append(this.eras_box);
    this.cursor = 0;
    this.choice = 0;
    this.available = [];
};



KnowledgeBox.prototype.setup = function (turn) {
    this.turn = turn;
    this.refresh();
};

var do_wheel_part = function (s, opts, part_name, replacements) {
    var era_name = opts.era_name,
        wheel = opts.wheel,
        purchase = opts.purchase && technology.get(opts.purchase),
        selection = opts.selection,
        tech_list = opts.tech_list,
        available = opts.available;

    var letters = _.str.chars(replacements);
    for (var name in wheel[part_name]) {
        if (name.indexOf('_') === 0) {
            continue;
        }

        var tech_name = era_name+"."+name;

        var i_available = _.indexOf(available, tech_name);
        var i_researched = _.indexOf(tech_list, tech_name);

        var tech = technology.get(tech_name);
        var label = tech.label().toUpperCase();
        var letter = letters.shift();

        var m = s.match(new RegExp(letter+"+"));
        var template = null;
        if (!letter || !m) {
            console.error("                      ", letter, label, m, s);
            throw "Unexpected number of technologies in era: " + era_name;
        }

        var length = m[0].length;

        if (purchase && (tech.full_name() === purchase.full_name())) {
            // selected
            template = '{cyan-bg}☑ %s{/cyan-bg}' ||
                        '{cyan-bg}>%s<{/cyan-bg}';
        } else if (i_available*1 === selection*1) {
            // selected
            template = '{green-bg}<%s>{/green-bg}';
        } else if (i_researched !== -1) {
            // already researched
            template = '{white-fg}[%s]{/white-fg}';
        } else if (i_available === -1) {
            // not yet researched and not available
            template = '{black-fg}[%s]{/black-fg}';
        } else {
            // not yet researched and available
            template = '{white-fg}(%s){/white-fg}'.replace("%i",
                        parseInt(i_available,10)+1);
        }

        label = _.str.rpad(label.substr(0, length-2), length-2);
        label = template.replace("%s", label);

        // insert
        s = s.substring(0, m.index)
            + label + s.substring(m.index+length);
    }
    return s;
};

KnowledgeBox.prototype.refresh = function () {
    this.box.focus();
    var current_era = this.turn.player.data.era;
    var tech_list = this.turn.player.data.technology;
    var purchase = this.turn.get_or_default(
                    actions.TYPE.PURCHASE_KNOWLEDGE).get('purchase');
    var purchase_tech = null;
    if (purchase) {
        purchase_tech = technology.get(purchase);
    }

    this.available = technology.Tech.available(current_era, tech_list);

    // make sure its cast down to int
    this.choice = this.choice || 0;

    //////////// Eras
    var s = "";
    var is_future = false;
    var padding = " ";
    for (var i in technology.era_list) {
        var era_name = technology.era_list[i];
        var era_label = _.str.humanize(era_name);
        if (current_era === era_name) {
            s += padding+"{green-fg}" + era_label + "{/green-fg}";
            is_future = true;
        } else if (is_future) {
            s += padding+"{black-fg}" + era_label + "{/black-fg}";
        } else {
            s += padding+"{white-fg}" + era_label + "{/white-fg}";
        }
        padding = "   ";
    }
    this.eras_box.setContent(s);


    //////////// Selection & Tech remaining
    s = civinfo.format_value("knowledge",
                this.turn.player.get("knowledge"));
    s += "\n\n{blue-fg}AVAILABLE:{/blue-fg}\n";
    var available = this.available;

    var path_number = function (path) {
            return (!path ? "¹" : "²") || '['+path+']';
    };

    for (var i in available) {
        var j = parseInt(i, 10);
        var tech = technology.get(available[i]);
        var line = '';


        if (purchase_tech && purchase_tech.full_name() === available[i]) {
            line += '{cyan-bg}☑  '
            if (purchase_tech.path !== null) {
                line += path_number(purchase_tech.path);
            }
            line += '{/cyan-bg}' || '[X]';
        }

        line += (j+1) + ". " + tech.label();
        if (this.cursor === j) {


            line = "{green-bg}" + line;
            // Display blurb
            if (tech.has_choice()) {
                line += " ";
                line += path_number(this.choice);
                line += "{/green-bg}";
                var choices = tech.data.choice;
                var wrappend = function (should, a) {
                    if (should) {
                        line += "[{green-fg}";
                        line += a;
                        line += "{/green-fg}]";
                    } else {
                        line += " " + a + " ";
                    }
                };
                line += "\nChoose a path: ";
                wrappend(this.choice === 0, choices[0].path_label || "Path A");
                line += "   -   ";
                wrappend(this.choice === 1, choices[1].path_label || "Path B");
                line += "\n";
                line += tech.blurb(this.opts, this.choice);
            } else {
                line += "{/green-bg}\n";
                line += tech.blurb(this.opts, null);
            }
            line += "\n\n";
        }
        s += line +"\n";
    }
    this.box.setContent(s);


    //////////// Wheel
    //var s = "{center}" + WHEEL_TEMPLATE + "{/center}";
    s = WHEEL_TEMPLATE;
    var o = {
        era_name: current_era,
        tech_list: tech_list,
        available: available,
        purchase: purchase,
        selection: this.cursor,
    };
    o.wheel = technology.eras[current_era].wheels[0];

    s = do_wheel_part(s, o, "center", "C");
    s = do_wheel_part(s, o, "core", "01");
    s = do_wheel_part(s, o, "left", "2468");
    s = do_wheel_part(s, o, "right", "3579");
    s = "{center}" + s + "{/center}";
    this.wheel_box.setContent(s);



    this.rerender();
    //this.box.render();
};


module.exports.KnowledgeBox = KnowledgeBox;

